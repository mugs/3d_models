$fn=50;

track_width = 136;
track_length = 108;
track_thickness = 6;

sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3;
rail_height = 10;

minkowski()
{
    difference() {
        union() {
            cube([track_width - sphere_diameter,track_length - sphere_diameter,track_thickness - sphere_diameter]);

            //convex jigsaw (top)
            translate([0, track_length, 0]) {
            
                difference() {
                    union() {
                        hull() {
                            //top-left circle
                            translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //top-right circle
                            translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //bottom-left circle
                            translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //bottom-right circle
                            translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                        }
                    }
                }
            }

            //convex jigsaw (bottom)
            translate([track_width - sphere_diameter, 0 - sphere_diameter, 0]) {
                rotate([0, 0, 180]) {
                    difference() {
                        union() {
                            hull() {
                                //top-left circle
                                translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //top-right circle
                                translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //bottom-left circle
                                translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //bottom-right circle
                                translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                            }
                        }
                    }
                }
            }
        }

        
        //concave jigsaw (top)
        translate([-3, track_length - sphere_radius - concave_buffer, 0]) {
            union() {
                hull() {
                    //top-left circle
                    translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    
                    //top-right circle
                    translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    
                    //bottom-left circle
                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    //bottom-right circle
                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1), 1, -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                }
                translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                    rotate([0, 0, 45]) {
                        cube([12,12,10]);
                    }
                }
                translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                    rotate([0, 0, 45]) {
                        cube([12,12,10]);
                    }
                }
            }
        }

        //concave jigsaw (bottom)
        translate([track_width, 0, 0]) {
            rotate([0, 0, 180]) {
                union() {
                    hull() {
                        //top-left circle
                        translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        
                        //top-right circle
                        translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        
                        //bottom-left circle
                        translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        //bottom-right circle
                        translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1),
                            1, -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                    }
                    translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                        rotate([0, 0, 45]) {
                            cube([12,12,10]);
                        }
                    }
                    translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                        rotate([0, 0, 45]) {
                            cube([12,12,10]);
                        }
                    }
                }
            }
        }
    }
    
    sphere(r=sphere_radius);
}


// Left rail
translate([rail_inset - sphere_radius, 0, track_thickness - sphere_radius - 1]) {
    cube([rail_thickness, track_length - sphere_diameter, rail_height + 1]);
}

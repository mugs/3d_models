width = 136;

first_ledge_lower_z = 6;
first_ledge_upper_z = 10.5;
first_ledge_upper_y = 8.5;

first_top_z = 15;
first_top_y = 6.5;

second_top_z = 21.8;
second_top_y = 21.5;

third_top_z = 21.8;
third_top_y = 40;

second_ledge_z = 16;
second_ledge_y1 = 40;
second_ledge_y2 = 48;


points = [
    //right side
    [width, 0, 0], // 0
    [width, 0, first_ledge_lower_z], // 1
    [width, first_ledge_upper_y, first_ledge_upper_z], // 2
    [width, first_top_y, first_top_z], // 3
    [width, second_top_y, second_top_z], // 4
    [width, third_top_y, third_top_z], // 5
    [width, second_ledge_y1, second_ledge_z], // 6
    [width, second_ledge_y2, second_ledge_z], // 7
    [width, second_ledge_y2, 0], // 8
    
    // left side
    [0, 0, 0], // 9
    [0, 0, first_ledge_lower_z], // 10
    [0, first_ledge_upper_y, first_ledge_upper_z], // 11
    [0, first_top_y, first_top_z], // 12
    [0, second_top_y, second_top_z], // 13
    [0, third_top_y, third_top_z], // 14
    [0, second_ledge_y1, second_ledge_z], // 15
    [0, second_ledge_y2, second_ledge_z], // 16
    [0, second_ledge_y2, 0] // 17
];

faces = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8], // right side
    [17, 16, 15, 14, 13, 12, 11, 10, 9], //left side
    [9, 10, 1, 0], //leading edge
    [10, 11, 2, 1],  //first ledge
    [11, 12, 3, 2], //track end
    [12, 13, 4, 3], //first top
    [13, 14, 5, 4], //second top
    [14, 15, 6, 5], //drop
    [15, 16, 7, 6], //second ledge
    [16, 17, 8, 7], //back edge
    [17, 9, 0, 8] //bottom

];

polyhedron(points, faces);
$fn=50;

track_width = 136;
track_length = 216;
track_thickness = 6;

sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3;
rail_height = 10;

minkowski()
{
    difference() {
        union() {
            cube([track_width - sphere_diameter,track_length - sphere_diameter,track_thickness - sphere_diameter]);

            //convex jigsaw (top)
            translate([0, track_length, 0]) {
            
                difference() {
                    union() {
                        hull() {
                            //top-left circle
                            translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //top-right circle
                            translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //bottom-left circle
                            translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                            //bottom-right circle
                            translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                            }
                        }
                    }
                }
            }

            //convex jigsaw (bottom)
            translate([track_width - sphere_diameter, 0 - sphere_diameter, 0]) {
                rotate([0, 0, 180]) {
                    difference() {
                        union() {
                            hull() {
                                //top-left circle
                                translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //top-right circle
                                translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //bottom-left circle
                                translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                                //bottom-right circle
                                translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                    cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                }
                            }
                        }
                    }
                }
            }
        }

        
        //concave jigsaw (top)
        translate([-3, track_length - sphere_radius - concave_buffer, 0]) {
            union() {
                hull() {
                    //top-left circle
                    translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    
                    //top-right circle
                    translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    
                    //bottom-left circle
                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                    //bottom-right circle
                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1), 1, -1]) {
                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                    }
                }
                translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                    rotate([0, 0, 45]) {
                        cube([12,12,10]);
                    }
                }
                translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                    rotate([0, 0, 45]) {
                        cube([12,12,10]);
                    }
                }
            }
        }

        //concave jigsaw (bottom)
        translate([track_width, 0, 0]) {
            rotate([0, 0, 180]) {
                union() {
                    hull() {
                        //top-left circle
                        translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        
                        //top-right circle
                        translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        
                        //bottom-left circle
                        translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                        //bottom-right circle
                        translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1),
                            1, -1]) {
                            cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                        }
                    }
                    translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                        rotate([0, 0, 45]) {
                            cube([12,12,10]);
                        }
                    }
                    translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                        rotate([0, 0, 45]) {
                            cube([12,12,10]);
                        }
                    }
                }
            }
        }
    }
    
    sphere(r=sphere_radius);
}

/*
// Left rail
translate([rail_inset - sphere_radius, 0, track_thickness - sphere_radius - 1]) {
    cube([rail_thickness, track_length - sphere_diameter, rail_height + 1]);
}

// Right rail
translate([ track_width - rail_inset - rail_thickness - sphere_radius, 0, track_thickness - sphere_radius - 1]) {
    cube([rail_thickness, track_length - sphere_diameter, rail_height + 1]);
}

center_rail_x = (track_width - sphere_diameter - rail_thickness) / 2;

// center rail
translate([ center_rail_x, 1, track_thickness - sphere_radius - 1]) {
    cube([rail_thickness, track_length - sphere_diameter - 2, rail_height + 1]);
}
*/

// 3 lanes
/*
//left
translate([ 0 - rail_thickness, 1, 0 - sphere_radius ])
{
    cube([rail_thickness, track_length - sphere_diameter - 2, rail_height + track_thickness]);
}

// right
translate([ 132.8, 1, 0 - sphere_radius ])
{
    cube([rail_thickness, track_length - sphere_diameter - 2, rail_height + track_thickness]);
}

// center_left
translate([ 43, 2 + jigsaw_length, track_thickness - sphere_radius - 1 ])
{
    cube([2.4, track_length - sphere_diameter - 4 - (jigsaw_length *2), rail_height + 1]);
}
// center_right
translate([ 43 + 42 + 2.4, 2 + jigsaw_length, track_thickness - sphere_radius - 1 ])
{
    cube([2.4, track_length - sphere_diameter - 4 - (jigsaw_length *2), rail_height + 1]);
}
*/

//Whoopdy
/*
whoopdy_radius = 25;
whoopdy_y = 108;

translate([0, whoopdy_y, whoopdy_radius + track_thickness - sphere_radius]) {
    rotate([90, 0, 90]) {
        cylinder(track_width - sphere_diameter, whoopdy_radius, whoopdy_radius, false);
    }
}

difference() {
    translate([0, whoopdy_y - (whoopdy_radius * 2), track_thickness - sphere_radius]) {
        cube([track_width - sphere_diameter, whoopdy_radius * 4, whoopdy_radius]);
    }

    union() {
        translate([0 - sphere_radius - 1, whoopdy_y - (whoopdy_radius * 2), whoopdy_radius + track_thickness - sphere_radius]) {
            rotate([90, 0, 90]) {
                cylinder(track_width + 2, whoopdy_radius, whoopdy_radius, false);
            }
        }
        translate([0 - sphere_radius - 1, whoopdy_y + (whoopdy_radius * 2), whoopdy_radius + track_thickness - sphere_radius]) {
            rotate([90, 0, 90]) {
                cylinder(track_width + 2, whoopdy_radius, whoopdy_radius, false);
            }
        }

    }
    
}
*/

// round ramp
ramp_radius = 60;
ramp_y = 108;
overlap = 18;

difference() {
    translate([0, ramp_y - (ramp_radius - (overlap / 2)), track_thickness - sphere_radius]) {
        cube([track_width - sphere_diameter, (2* ramp_radius) - overlap, ramp_radius]);
    }
    
    union() {
        translate([0 - sphere_radius - 1, ramp_y - (ramp_radius - (overlap / 2)), ramp_radius + track_thickness - sphere_radius]) {
            rotate([90, 0, 90]) {
                cylinder(track_width + 2, ramp_radius, ramp_radius, false);
            }
        }
        translate([0 - sphere_radius - 1, ramp_y + (ramp_radius - (overlap / 2)), ramp_radius + track_thickness - sphere_radius]) {
            rotate([90, 0, 90]) {
                cylinder(track_width + 2, ramp_radius, ramp_radius, false);
            }
        }

    }
    
}




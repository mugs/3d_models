
support_width = 14;
support_high_height = 35;
support_low_height = 22;
support_depth = 20;


vertical_thickness = 2;
horizontal_thickness = 2;
brace_height = 7;
brace_thickness = 2;

track_height = 30; // height off ground
track_width = 140;
track_thickness = 6.4;
track_overhang_width = 5;
track_overhang_thickness = 1.8;

points = [
    //front_face
    [0, 0, 0], //0 
    [0, 0, support_low_height], //1
    [support_width, 0, support_high_height], //2 
    [support_width, 0, 0],  //3 
    
    //back face
    [support_width, support_depth, 0], //4 
    [support_width, support_depth, support_high_height], //5 
    [0, support_depth, support_low_height], //6 
    [0, support_depth, 0], //7 
    
];

faces = [
    [0, 1, 2, 3], //front face
    [4, 5, 6, 7], //back face
    [0, 7, 6, 1], // left side
    [1, 6, 5, 2], // top
    [2, 5, 4, 3], // right side
    [3, 4, 7, 0], // bottom
];


polyhedron(points, faces);



difference() {
    
    union() {
        
        // left support
        polyhedron(points, faces);
        
        //left wall
        vertical_height = track_height + track_thickness + track_overhang_thickness;
        translate([support_width, 0, 0]) {
            cube([vertical_thickness, support_depth, vertical_height]);
            
        }
        
        // track base
        translate([support_width + vertical_thickness, 0, track_height - horizontal_thickness]) {
            cube([track_width, support_depth, horizontal_thickness]);
            
        }
        
        // front brace
        translate([support_width + vertical_thickness, 0, track_height - horizontal_thickness - brace_height]) {
            cube([track_width, brace_thickness, brace_height]); 
        }

        // rear brace
        translate([support_width + vertical_thickness, support_depth - brace_thickness, track_height - horizontal_thickness - brace_height]) {
            //cube([track_width, brace_thickness, brace_height]); 
        }

        
        // left overhang
        translate([support_width + vertical_thickness, 0, track_height + track_thickness]) {
            cube([track_overhang_width, support_depth, track_overhang_thickness]);
            
        }
        
        // right overhang
        translate([support_width + vertical_thickness + track_width - track_overhang_width, 0, track_height + track_thickness]) {
            cube([track_overhang_width, support_depth, track_overhang_thickness]);
            
        }
        
        // right wall
        translate([support_width + vertical_thickness + track_width, 0, 0]) {
            cube([vertical_thickness, support_depth, vertical_height]);
        }
        
        //right support
        translate([support_width + vertical_thickness + track_width + vertical_thickness + support_width, support_depth, 0]) {
            rotate([0, 0, 180]) {
                
                polyhedron(points, faces);
            }
        }
        
        
    }
    
    
    
    union() {
        
        
    }
}


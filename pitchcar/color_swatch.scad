
thickness = 2;
corner_roundness = 5;

total_x = 64;
total_y = 28;

hole_size = 2;

brand = "Hatchbox";
color = "Pink";

difference() {
    union() {
        hull() {
            translate([corner_roundness, corner_roundness, thickness / 2]) {
                cylinder(thickness, corner_roundness, corner_roundness, true);
            }
            translate([corner_roundness, total_y - corner_roundness, thickness / 2]) {
                cylinder(thickness, corner_roundness, corner_roundness, true);
            }
            translate([total_x - corner_roundness, corner_roundness, thickness / 2]) {
                cylinder(thickness, corner_roundness, corner_roundness, true);
            }
            translate([total_x - corner_roundness, total_y - corner_roundness, thickness / 2]) {
                cylinder(thickness, corner_roundness, corner_roundness, true);
            }
        }
        
            
        
    }
    
    union() {
        translate([5, total_y - 10, thickness / 2]) {
            linear_extrude(thickness) {
                text(brand, "Liberation Sans", size = 5, "center");
            }
        }
        translate([5, total_y - 17, thickness / 2]) {
            linear_extrude(thickness) {
                text(color, "Liberation Sans", size = 5, "center");
            }
        }

        translate([total_x - corner_roundness, total_y - corner_roundness, -1]) {
            cylinder(thickness + 2, hole_size, hole_size, false);
        }
    }
    
    
}



$fn=50;

track_width = 136;
track_length = 197;
track_thickness = 6;

sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3;
rail_height = 10;

rail_inset = 9;
rail_thickness = 3.3;
rail_height = 10;
rail_z_offset = 2.2;

difference() {
        
    translate([sphere_radius, sphere_radius, sphere_radius]) {
        minkowski()
        {
            difference() {
                union() {
                    cube([track_width - sphere_diameter,track_length - sphere_diameter,track_thickness - sphere_diameter]);

                }

                
            }
            sphere(r=sphere_radius);
        }
    }

    union() {
        translate([rail_inset, -1, rail_z_offset]) {
            cube([rail_thickness, track_length + 2, rail_height]);
        }
        translate([track_width - rail_inset - rail_thickness, -1, rail_z_offset]) {
            cube([rail_thickness, track_length + 2, rail_height]);
        }
    }

}


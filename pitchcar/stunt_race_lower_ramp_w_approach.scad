width = 136;
first_peak_y = 16.5;
first_peak_z = 12.5;
track_height = 6;
first_trough_y = 19;
first_trough_z = 6.75;
second_peak_y = 40;
second_peak_z = 15.75;


$fn=50;

track_width = 136;
track_length = 99;
track_thickness = 6;

sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3.3;
rail_height = 10;

difference() {
    union() {
        translate([sphere_radius, sphere_radius, sphere_radius]) {
            minkowski()
            {
                difference() {
                    union() {
                        cube([track_width - sphere_diameter,track_length - sphere_diameter,track_thickness - sphere_diameter]);

                        //convex jigsaw (top)
                        translate([0, track_length, 0]) {
                        
                            difference() {
                                union() {
                                    hull() {
                                        //top-left circle
                                        translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                            cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                        }
                                        //top-right circle
                                        translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                            cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                        }
                                        //bottom-left circle
                                        translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                            cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                        }
                                        //bottom-right circle
                                        translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                            cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                        }
                                    }
                                }
                            }
                        }

                    }

                    
                    //concave jigsaw (top)
                    translate([-3, track_length - sphere_radius - concave_buffer, 0]) {
                        union() {
                            hull() {
                                //top-left circle
                                translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                
                                //top-right circle
                                translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                
                                //bottom-left circle
                                translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                //bottom-right circle
                                translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1), 1, -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                            }
                            translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                                rotate([0, 0, 45]) {
                                    cube([12,12,10]);
                                }
                            }
                            translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                                rotate([0, 0, 45]) {
                                    cube([12,12,10]);
                                }
                            }
                        }
                    }

                }
                
                sphere(r=sphere_radius);
            }

        }


        points = [
            //leading edge
            [0, 0, 0],
            [width, 0, 0],
            [0, 0, track_height],
            [width, 0, track_height],
            
            //right side
            [width, first_peak_y, first_peak_z],
            [width, first_trough_y, first_trough_z],
            [width, second_peak_y, second_peak_z],
            [width, second_peak_y, 0],
            
            //left side
            [0, first_peak_y, first_peak_z],
            [0, first_trough_y, first_trough_z],
            [0, second_peak_y, second_peak_z],
            [0, second_peak_y, 0]    
        ];

        faces = [
            [0, 2, 3, 1], //leading edge
            [3, 4, 5, 6, 7, 1], //right side
            [0, 11, 10, 9, 8, 2], //left side
            [10, 11, 7, 6], //back edge
            [0, 1, 7, 11], //bottom
            [8, 4, 3, 2], // first slope
            [4, 8, 9, 5], //downslope
            [10, 6, 5, 9] // second slope
        ];

        translate([width, 0, 0]) {
            rotate([0, 0, 180]) {
                polyhedron(points, faces);
            }
        }

        translate([0, 0, 0]) {
            cube([width, sphere_radius, track_thickness]);
        }
    }
    
    union(){
        translate([rail_inset, -2, 2]) {
            cube([rail_thickness, track_length + 3, track_height]);
        }
        translate([track_width - rail_inset - rail_thickness, 0, 2]) {
            cube([rail_thickness, track_length + 3, track_height]);
        }

        translate([rail_inset + rail_thickness, -2, 1]) {
            rotate([25, 0, 180]) {
                cube([rail_thickness, track_length, rail_height]);
            }
        }

        translate([track_width - rail_inset, -2, 1]) {
            rotate([25, 0, 180]) {
                cube([rail_thickness, track_length, rail_height]);
            }
        }
    }
}


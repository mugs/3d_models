$fn=50;

track_width = 136;

track_length = 216; 
track_thickness = 6;

loop_cutoff = 0;


sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3;
rail_height = 10;

union() { 
    
    translate([-0.5, 108 + 17.5 + 1.1, 0.2]) {
        rotate([90, 0, 0]) {
            import("LOOP-RL2b.STL");
        }
    }


    translate([sphere_radius, sphere_radius + loop_cutoff, sphere_radius]) {
        minkowski()
        {
            difference() {
                union() {
                    cube([track_width - sphere_diameter,track_length - sphere_diameter - loop_cutoff,track_thickness - sphere_diameter]);

                    //convex jigsaw (top)
                    translate([0, track_length - loop_cutoff, 0]) {
                    
                        difference() {
                            union() {
                                hull() {
                                    //top-left circle
                                    translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                        cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                    }
                                    //top-right circle
                                    translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                        cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                    }
                                    //bottom-left circle
                                    translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                        cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                    }
                                    //bottom-right circle
                                    translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                        cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                    }
                                }
                            }
                        }
                    }
                }

                union() {
                    //jigsaw bottom
                    translate([20, 0, 0]) {
                        cube([50, 30, track_thickness]);
                    }

                    
                    //concave jigsaw (top)
                    translate([-3, track_length - sphere_radius - concave_buffer - loop_cutoff, 0]) {
                        union() {
                            hull() {
                                //top-left circle
                                translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                
                                //top-right circle
                                translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                
                                //bottom-left circle
                                translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                                //bottom-right circle
                                translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1), 1, -1]) {
                                    cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                }
                            }
                            translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                                rotate([0, 0, 45]) {
                                    cube([12,12,10]);
                                }
                            }
                            translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                                rotate([0, 0, 45]) {
                                    cube([12,12,10]);
                                }
                            }
                        }
                    }
                }
            }
            sphere(r=sphere_radius);
        }
    }
}


inner_rail_start_y = (4 + jigsaw_length);
outer_branch_start_y = 75;
inner_branch_start_y = 100;
inner_branch_end_y = 100;
outer_branch_end_y = 145;

color("purple") {
    
    x1 = 2;
    x2 = x1 + rail_thickness;
    x3 = rail_inset;
    x4 = rail_inset + rail_thickness;
    y1 = 53;
    y2 = track_length - sphere_radius;
    z1 = track_thickness;
    z2 = track_thickness + rail_height;
    
    points1 = [
        
        //bottom
        [x1, y1, z1], // 0
        [x2, y1, z1], // 1
        [x4, y2, z1], // 2
        [x3, y2, z1], // 3
        
        // top
        [x1, y1, z2], // 4
        [x2, y1, z2], // 5
        [x4, y2, z2], // 6
        [x3, y2, z2], // 7
    ];

    faces1 = [
        [0, 4, 5, 1], //front face
        [1, 5, 6, 2], // outside face
        [2, 6, 7, 3], // back face
        [3, 7, 4, 0], // inside face
        [7, 6, 5, 4], // top
        [0, 1, 2, 3], // bottom        
    ];

    polyhedron(points1, faces1);
}



color("red") {
    
    x1 = 63;
    x2 = x1 + rail_thickness;
    x3 = track_width - rail_thickness - rail_inset;
    x4 = x3 + rail_thickness;
    y1 = 90;
    y2 = track_length - sphere_radius;
    z1 = track_thickness;
    z2 = track_thickness + rail_height;
    
    points2 = [
        
        //bottom
        [x1, y1, z1], // 0
        [x2, y1, z1], // 1
        [x4, y2, z1], // 2
        [x3, y2, z1], // 3
        
        // top
        [x1, y1, z2], // 4
        [x2, y1, z2], // 5
        [x4, y2, z2], // 6
        [x3, y2, z2], // 7
    ];

    faces2 = [
        [0, 4, 5, 1], //front face
        [1, 5, 6, 2], // outside face
        [2, 6, 7, 3], // back face
        [3, 7, 4, 0], // inside face
        [7, 6, 5, 4], // top
        [0, 1, 2, 3], // bottom        
    ];

    polyhedron(points2, faces2);
}



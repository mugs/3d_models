width = 136;
first_peak_y = 16.5;
first_peak_z = 12.5;
track_height = 6;
first_trough_y = 19;
first_trough_z = 6.75;
second_peak_y = 40;
second_peak_z = 15.75;

points = [
    //leading edge
    [0, 0, 0],
    [width, 0, 0],
    [0, 0, track_height],
    [width, 0, track_height],
    
    //right side
    [width, first_peak_y, first_peak_z],
    [width, first_trough_y, first_trough_z],
    [width, second_peak_y, second_peak_z],
    [width, second_peak_y, 0],
    
    //left side
    [0, first_peak_y, first_peak_z],
    [0, first_trough_y, first_trough_z],
    [0, second_peak_y, second_peak_z],
    [0, second_peak_y, 0]    
];

faces = [
    [0, 2, 3, 1], //leading edge
    [3, 4, 5, 6, 7, 1], //right side
 //   [2, 8, 9, 10, 11, 0], //left side
    [0, 11, 10, 9, 8, 2],
    [6, 7, 11, 10], //back edge
    [0, 1, 7, 11], //bottom
    [2, 3, 4, 8], // first slope
    [4, 8, 9, 5], //downslope
    [9, 5, 6, 10] // second slope
];

polyhedron(points, faces);
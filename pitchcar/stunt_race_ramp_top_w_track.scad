width = 136;

first_ledge_lower_z = 6;
first_ledge_upper_z = 10.5;
first_ledge_upper_y = 8.5;

first_top_z = 15;
first_top_y = 6.5;

second_top_z = 21.8;
second_top_y = 21.5;

third_top_z = 21.8;
third_top_y = 40;

second_ledge_z = 15.8;
second_ledge_y1 = 40;
second_ledge_y2 = 48;


$fn=50;

track_width = 136;
track_length = 99;
track_thickness = 6;

sphere_radius = 1.6;
sphere_diameter = 2 * sphere_radius;

bottom = 0 - sphere_radius;
left = bottom;
top = track_thickness - sphere_radius;
right = track_width - sphere_radius;

jigsaw_length = 17.5;
jigsaw_width = 35.5;

jigsaw_from_edge = 26;
jigsaw_bottom_from_edge = 31.5;
jigsaw_circle_radius = 5;

concave_buffer = 1;

rail_inset = 9;
rail_thickness = 3.3;
rail_height = 10;

difference() {
    union() {
        translate([0, second_ledge_y1, second_ledge_z]) {
            translate([sphere_radius, sphere_radius, sphere_radius]) {
                minkowski()
                {
                    difference() {
                        union() {
                            cube([track_width - sphere_diameter,track_length - sphere_diameter,track_thickness - sphere_diameter]);

                            //convex jigsaw (top)
                            translate([0, track_length, 0]) {
                            
                                difference() {
                                    union() {
                                        hull() {
                                            //top-left circle
                                            translate([jigsaw_from_edge + jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                            }
                                            //top-right circle
                                            translate([jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius, jigsaw_length - jigsaw_circle_radius, 0]) {
                                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                            }
                                            //bottom-left circle
                                            translate([jigsaw_bottom_from_edge + jigsaw_circle_radius, -1, 0]) {
                                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                            }
                                            //bottom-right circle
                                            translate([jigsaw_bottom_from_edge + jigsaw_width - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius, -1, 0]) {
                                                cylinder(track_thickness - sphere_diameter, jigsaw_circle_radius - sphere_radius, jigsaw_circle_radius - sphere_radius, false);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        
                        //concave jigsaw (top)
                        translate([-3, track_length - sphere_radius - concave_buffer, 0]) {
                            union() {
                                hull() {
                                    //top-left circle
                                    translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                    }
                                    
                                    //top-right circle
                                    translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius), 0 - (jigsaw_length - jigsaw_circle_radius), -1]) {
                                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                    }
                                    
                                    //bottom-left circle
                                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_circle_radius)  + concave_buffer + 1, 1, -1]) {
                                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                    }
                                    //bottom-right circle
                                    translate([track_width - (jigsaw_bottom_from_edge + jigsaw_width + concave_buffer - (2*(jigsaw_bottom_from_edge-jigsaw_from_edge)) - jigsaw_circle_radius + 1), 1, -1]) {
                                        cylinder(10, jigsaw_circle_radius + sphere_radius + concave_buffer, jigsaw_circle_radius + sphere_radius + concave_buffer, false);
                                    }
                                }
                                translate([track_width - (jigsaw_from_edge + jigsaw_circle_radius) + 2, -7.5, -1]) {
                                    rotate([0, 0, 45]) {
                                        cube([12,12,10]);
                                    }
                                }
                                translate([track_width - (jigsaw_from_edge + jigsaw_width - jigsaw_circle_radius) - 2, -7.5, -1]) {
                                    rotate([0, 0, 45]) {
                                        cube([12,12,10]);
                                    }
                                }
                            }
                        }

                    }
                    
                    sphere(r=sphere_radius);
                }

            }
        }


        points = [
            //right side
            [width, 0, 0], // 0
            [width, 0, first_ledge_lower_z], // 1
            [width, first_ledge_upper_y, first_ledge_upper_z], // 2
            [width, first_top_y, first_top_z], // 3
            [width, second_top_y, second_top_z], // 4
            [width, third_top_y, third_top_z], // 5
            [width, second_ledge_y1, second_ledge_z], // 6
            [width, second_ledge_y2, second_ledge_z], // 7
            [width, second_ledge_y2, 0], // 8
            
            // left side
            [0, 0, 0], // 9
            [0, 0, first_ledge_lower_z], // 10
            [0, first_ledge_upper_y, first_ledge_upper_z], // 11
            [0, first_top_y, first_top_z], // 12
            [0, second_top_y, second_top_z], // 13
            [0, third_top_y, third_top_z], // 14
            [0, second_ledge_y1, second_ledge_z], // 15
            [0, second_ledge_y2, second_ledge_z], // 16
            [0, second_ledge_y2, 0] // 17
        ];

        faces = [
            [0, 1, 2, 3, 4, 5, 6, 7, 8], // right side
            [17, 16, 15, 14, 13, 12, 11, 10, 9], //left side
            [9, 10, 1, 0], //leading edge
            [10, 11, 2, 1],  //first ledge
            [11, 12, 3, 2], //track end
            [12, 13, 4, 3], //first top
            [13, 14, 5, 4], //second top
            [14, 15, 6, 5], //drop
            [15, 16, 7, 6], //second ledge
            [16, 17, 8, 7], //back edge
            [17, 9, 0, 8] //bottom

        ];

        polyhedron(points, faces);

        translate([0, 0, 0]) {
            cube([width, sphere_radius, track_thickness]);
        }

        translate([0, second_ledge_y1, 0]) {
            cube([width, second_ledge_y2 - second_ledge_y1, second_ledge_z + track_thickness]);
        }
        
        translate([0, second_ledge_y1, 8]) {
            cube([width, 30, second_ledge_z + track_thickness - 8 - sphere_radius]);
        }

    }
    
    union(){
        translate([0, 0, second_ledge_z]) {
            translate([rail_inset, 0, 2]) {
                cube([rail_thickness, track_length * 2, rail_height]);
            }
            translate([track_width - rail_thickness - rail_inset, 0, 2]) {
                cube([rail_thickness, track_length * 2, rail_height]);
            }
        }

        translate([track_width - rail_inset - rail_thickness, -2, first_ledge_lower_z + 1]) {
            rotate([25, 0, 0]) {
                cube([rail_thickness, track_length, rail_height]);
            }
        }
        
        translate([rail_inset, -2, first_ledge_lower_z + 1]) {
            rotate([25, 0, 0]) {
                cube([rail_thickness, track_length, rail_height]);
            }
        }


    }
    
}




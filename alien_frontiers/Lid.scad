include <prism.scad>


dice_size = 16;
dice_side_gap = 3;
dice_y_gap = 1;
dice_top_gap = 1;

wall_thickness = 2;
floor_thickness = 2;
top_thickness = 3;
colony_radius = 10;
colony_gap = 2;
colony_height = 13;
colony_wall_height = 4;
colony_area_y = 44;


lock_post_width = 10;
sphere_radius = 5;

total_x = (6*dice_size) + dice_side_gap + (wall_thickness *2);
total_y = (2*wall_thickness) + dice_size + dice_y_gap + colony_area_y;
total_z = dice_size + floor_thickness + top_thickness + dice_top_gap;

cavity_x = ((6*dice_size) + dice_side_gap);




difference() {
    cube([total_x, total_y, total_z]);


    union() {
        //dice cavity
        translate([wall_thickness, (wall_thickness + colony_area_y), floor_thickness]) {
            cube([cavity_x, (dice_size + dice_y_gap), (dice_size + top_thickness + dice_top_gap + 1)]);
        }
        
        //lid
        translate([wall_thickness, (wall_thickness), total_z - top_thickness]) {
            cube([total_x, total_y - (wall_thickness * 2), top_thickness + 1]);
        }

        translate([wall_thickness, wall_thickness + .0001, total_z - top_thickness]) {
            rotate([0, 0, 270]) {
                prism(total_x, 1.2, top_thickness);
            }
        }
        translate([wall_thickness + total_x, total_y - wall_thickness - .0001, total_z - top_thickness]) {
            rotate([0, 0, 90]) {
                prism(total_x, 1.2, top_thickness);
            }
        }

        
        //Big colony cavity
        translate([wall_thickness, wall_thickness, (total_z - (top_thickness + dice_top_gap + (colony_height - colony_wall_height)))]) {
            cube([cavity_x, colony_area_y + 1, (top_thickness + dice_top_gap + (colony_height - colony_wall_height) + 1)]);
        }
        
        
        // small colony cavities
        translate([wall_thickness + colony_radius + 1, wall_thickness + colony_radius + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + colony_radius + ((colony_radius*2) * 1) + (colony_gap * 1) + 1, wall_thickness + colony_radius + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + colony_radius + ((colony_radius*2) * 2) + (colony_gap * 2) + 1, wall_thickness + colony_radius + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + colony_radius + ((colony_radius*2) * 3) + (colony_gap * 3) + 1, wall_thickness + colony_radius + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        
        translate([wall_thickness + (colony_radius*2) + 1, wall_thickness + (colony_radius*3) + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + (colony_radius*2) + ((colony_radius*2) * 1) + (colony_gap * 1) + 1, wall_thickness + (colony_radius*3) + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + (colony_radius*2) + ((colony_radius*2) * 2) + (colony_gap * 2) + 1, wall_thickness + (colony_radius*3) + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        translate([wall_thickness + (colony_radius*2) + ((colony_radius*2) * 3) + (colony_gap * 3) + 1, wall_thickness + (colony_radius*3) + 1, (total_z - (top_thickness + dice_top_gap + colony_height))]) {
            cylinder(dice_top_gap + colony_height, colony_radius, colony_radius);
        }
        
        
        
        
    }
 
}

translate([wall_thickness, (total_y - lock_post_width) / 2, 0] ) {
    cube([lock_post_width, lock_post_width, total_z - top_thickness]);
}

translate([wall_thickness + (lock_post_width/2), (total_y) / 2, total_z - sphere_radius - 1.5] ) {
    sphere(sphere_radius);
}



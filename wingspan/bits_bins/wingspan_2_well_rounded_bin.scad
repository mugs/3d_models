include <roundedcube.scad>


$fn = $preview ? 12 : 0;

total_z = 40;

outer_box_roundedness = 4;
inner_box_roundedness = 3;

box_side_thickness = 2.8;
box_bottom_thickness = 1.4;
box_top_thickness = 1.4;
lip_thickness = 1.4;
lip_height = 10;

outer_box_total_x = 142;
outer_box_total_y = 71;
outer_box_total_z = total_z - box_top_thickness - lip_height;


inner_box_total_x = outer_box_total_x - ((box_side_thickness - lip_thickness) * 2);
inner_box_total_y = outer_box_total_y - ((box_side_thickness - lip_thickness) * 2);
inner_box_total_z = total_z - box_top_thickness;


z_seam_slot_width = 3;
z_seam_slot_depth = 0.5;

cavity_1_x = 62;
cavity_roundedness = 4;

inner_wall_thickness = 1.2;
slide_buffer = 0.4;



difference() {
	union() {
		
        //outer box
        translate([0, 0, 0 - outer_box_roundedness]) { 
            difference() { 
                roundedcube([outer_box_total_x, outer_box_total_y, outer_box_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
            
                
                union() {
                    cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    translate([0, 0, outer_box_total_z + outer_box_roundedness]) {
                        cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    }
                }
                
            }
        }

        //inner box
        translate([box_side_thickness - lip_thickness, box_side_thickness - lip_thickness, 0 - inner_box_roundedness]) { 
            difference() { 
                roundedcube([inner_box_total_x, inner_box_total_y, inner_box_total_z +  (inner_box_roundedness * 2)], false, inner_box_roundedness);
            
                
                union() {
                    cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
                    translate([0, 0, inner_box_total_z + inner_box_roundedness]) {
                        cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
                    }
                }
                
            }
        }	

    }
    
    
    union() {
        //cavity 1
        translate([box_side_thickness, box_side_thickness, box_bottom_thickness]) {
			roundedcube([cavity_1_x, outer_box_total_y - (2* box_side_thickness), outer_box_total_z * 2], false, cavity_roundedness);
        }
        
        //cavity 2
        translate([box_side_thickness + cavity_1_x + inner_wall_thickness, box_side_thickness, box_bottom_thickness]) {
            cavity_2_x = outer_box_total_x - cavity_1_x - inner_wall_thickness - (2* box_side_thickness);
			roundedcube([cavity_2_x, outer_box_total_y - (2* box_side_thickness), outer_box_total_z * 2], false, cavity_roundedness);
        }
        
        
        
        //z-seam slot
        translate([
                box_side_thickness - lip_thickness, 
                (outer_box_total_y - z_seam_slot_width) / 2,
                outer_box_total_z
            ]) {
            
            cube([z_seam_slot_depth, z_seam_slot_width, 100]);
                
        }
        
    }
    
}


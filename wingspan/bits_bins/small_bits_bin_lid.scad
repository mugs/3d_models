include <card_tray_lid.scad>

finger_hole_radius = 10;

difference() {
    card_tray_lid(
        card_size = [93, 59, 32],
        bottom_text = "Bits Bits Bits",
        wall_x = 70,
        wall_y = 50,
        text_size = 8
    );

    translate([50, 26, 36]) {
        rotate([90, 0, 0]) {
            cylinder(h = 110, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
        }
    }
}



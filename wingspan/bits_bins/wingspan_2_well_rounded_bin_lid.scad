include <roundedcube.scad>


$fn = $preview ? 12 : 0;

total_z = 40;

outer_box_roundedness = 4;
inner_box_roundedness = 3;

box_side_thickness = 2.8;
box_bottom_thickness = 1.4;
box_top_thickness = 1.4;
lip_thickness = 1.4;
lip_height = 10;

outer_box_total_x = 142;
outer_box_total_y = 71;
lid_total_z = box_top_thickness + lip_height;


inner_box_total_x = outer_box_total_x - ((box_side_thickness - lip_thickness) * 2);
inner_box_total_y = outer_box_total_y - ((box_side_thickness - lip_thickness) * 2);
inner_box_total_z = total_z - box_top_thickness;


z_seam_slot_width = 3;
z_seam_slot_depth = 0.5;
slide_buffer = 0.4;

cavity_total_x = inner_box_total_x + slide_buffer;
cavity_total_y = inner_box_total_y + slide_buffer;

inner_wall_thickness = 1.2;



difference() {
	union() {
		
        //outer box
        translate([0, 0, 0 - outer_box_roundedness]) { 
            difference() { 
                roundedcube([outer_box_total_x, outer_box_total_y, lid_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
            
                
                union() {
                    cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    translate([0, 0, lid_total_z + outer_box_roundedness]) {
                        cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    }
                }
                
            }
        }        
    }

	union(){

        //cavity 
        translate([
            box_side_thickness - lip_thickness - (slide_buffer / 2), 
            box_side_thickness - lip_thickness - (slide_buffer / 2), 
            0 - inner_box_roundedness + box_bottom_thickness
        ]) { 
            difference() { 
                roundedcube([cavity_total_x, cavity_total_y, 1000], false, inner_box_roundedness);
            
                
                union() {
                    cube([cavity_total_x, cavity_total_y, inner_box_roundedness]);
                }
                
            }
        }

        //z-seam slot
        translate([
                box_side_thickness - lip_thickness - z_seam_slot_depth, 
                (outer_box_total_y - z_seam_slot_width) / 2,
                box_bottom_thickness
            ]) {
            
            cube([z_seam_slot_depth, z_seam_slot_width, 100]);
                
        }

/*
		// bottom text
		translate([outer_box_total_x / 2, outer_box_total_y / 2, 0.95]) {
			rotate([180, 0, 180]) {
				linear_extrude(1) {
					text(game_title, bottom_text_size, halign="center");
				}
			}
		}
		
*/
	}

}
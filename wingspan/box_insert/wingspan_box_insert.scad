board_x = 202;
board_y = 283;
board_z = 16;

bottom_group_x = 282;
bottom_group_y = 178;
bottom_group_y_offset = 10;

top_group_x = 267;
top_group_y = 106;


floor_thickness = 1;
total_x = 304;
total_y = 304;
total_z = 87;

kill_wall_x = 80;
kill_wall_y = 200;

kill_floor_xy = 180;

difference() {
    
    cube([total_x, total_y, total_z]);
    
    
    union() {
        translate([(total_x - board_x) / 2, (total_y - board_y) / 2, floor_thickness]) {
            cube([board_x, board_y, total_z]);
        }
        
        translate([(total_x - bottom_group_x) / 2, bottom_group_y_offset, floor_thickness + board_z]) {
            cube([bottom_group_x, bottom_group_y, total_z]);
            
        }
        
        translate([(total_x - top_group_x) / 2, bottom_group_y_offset + bottom_group_y, floor_thickness + board_z]) {
            cube([top_group_x, top_group_y, total_z]);
            
        }
        
        
        
        kill_wall_x_offset = (total_x - kill_wall_x) / 2;
        translate([kill_wall_x_offset, 0, floor_thickness]) {
            cube([kill_wall_x, total_y, total_z]);
            
        }

        //y_offset4 = (total_y - kill_wall_y) / 2;
        //translate([0, y_offset4, floor_thickness]) {
        //    cube([total_x, kill_wall_y, total_z]);
        //    
        //}
        
        kill_floor_offset = (total_x - kill_floor_xy) / 2;
        translate([kill_floor_offset, kill_floor_offset, 0]) {
            cube([kill_floor_xy, kill_floor_xy, total_z]);
            
        }

        
    }
    
    
    
    
    
    
}
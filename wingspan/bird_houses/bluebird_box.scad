
$fn = $preview ? 12 : 72;

back_height = 17;
back_thickness = 1;
back_width = 10;

base_width = 9;
base_depth = 9;

house_back_height = 15;
roof_thickness = 1;
roof_length = 11.4;

perch_radius = .65;
perch_length = 1;
perch_height = 5;


cavity_radius = 1.65;
cavity_height = 8.5;


difference() {
    union() {
        //back
        translate([0 - (back_width - base_width)/2, 0, 0]) {
        	cube([back_width, back_thickness, back_height]);
        }
    	
    	//main house
    	difference() {
	    	cube([base_width, base_depth, house_back_height]);
    	
			translate([0 - (back_width - base_width)/2, 0, house_back_height - roof_thickness]) {

				rotate([-15, 0, 0]) {
					cube([back_width, roof_length,  100]);
				}
			}    	
    	
    	}
    	
    		
    	//roof
		translate([0 - (back_width - base_width)/2, 0, house_back_height - roof_thickness]) {

			rotate([-15, 0, 0]) {
				cube([back_width, roof_length,  roof_thickness]);
			}
		}    	
    
    	//perch
    	translate([base_width / 2, base_depth + perch_length, perch_height]) {
            rotate([90, 0, 0]) {
                cylinder(h=perch_length,r=perch_radius);
            }
        }
        
    }
    
    union() {
    	//cavity
    	translate([base_width / 2, base_depth + .1, cavity_height]) {
            rotate([90, 0, 0]) {
                cylinder(h=base_depth-1,r=cavity_radius);
            }
        }

        
    }
    
}








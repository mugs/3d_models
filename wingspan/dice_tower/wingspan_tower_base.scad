include <roundedcube.scad>

x_size = 174;
y_size = 77;
z_size = 25.4;

back_wall_thickness = 1.8;
side_wall_thickness = 2;

floor_thickness = 1.6;

side_wall_height = 16;



difference() {
    cube([x_size, y_size, z_size]);
    
    
    union() {
        
        translate([back_wall_thickness + 60, side_wall_thickness, floor_thickness]) {
            roundedcube([x_size - side_wall_thickness - back_wall_thickness - 60,
                y_size - (2*side_wall_thickness), 100], false, 4);
        }
        translate([back_wall_thickness, (y_size - 65) / 2, floor_thickness]) {
            cube([80, 65, 50]);
        }
        dip_x_size = (x_size - (back_wall_thickness + 60 + side_wall_thickness + 20));

        translate([back_wall_thickness + 60 + 10, -20, side_wall_height]) {
            roundedcube([dip_x_size,
                y_size + 40, 100], false, 10);
        }

        translate([back_wall_thickness + 60 + dip_x_size, -20, 22]) {
            cube([100, 100, 100]);
        }


    }
}



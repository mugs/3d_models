include <prism.scad>

box_length = 135;
box_width = 101;
box_height = 45;
extension_length = 15;

end_grip_length = 43;


side_grip_height = 4;
side_grip_width = 2;

x_wall_thickness = 3;
y_wall_thickness = 6;
floor_thickness = 1.4;
extension_thickness = 4;

corner_width = 20;
bottom_barrier_height = 20;

bottom_hole_size = 40;




difference() {
    union() {
        cube([box_length, box_width, box_height]);
        translate([box_length - 1, 0, 0]){
            cube([extension_length + 1, box_width, extension_thickness]);

        }
        

        translate([0, -1.6, 10]) {
            rotate([0, 225, 270]) {
                prism(box_length, 3, 3);
            }
        }
        translate([0, -1.6, 20]) {
            rotate([0, 225, 270]) {
                prism(box_length, 3, 3);
            }
        }
        
         translate([box_length + 1, -1.3, 2]) {
            rotate([0, 225, 270]) {
                prism(extension_length - 2, 2, 2);
            }
        }

        
        translate([box_length + extension_length + 1.3, 0, 2]) {
            rotate([0, 225, 0]) {
                prism(end_grip_length, 2, 2);
            }
        }

        
    }
    
    
    union() {
        translate([x_wall_thickness, y_wall_thickness, floor_thickness]) {
            cube([box_length - (2*x_wall_thickness),
                box_width - (2*y_wall_thickness),
                box_height]);
        };


        translate([-1, -1.7 + box_width, 10]) {
            rotate([0, 225, 270]) {
                prism(box_length + 2, 3, 3);
            }
        }
        translate([-1, -1.7 + box_width, 20]) {
            rotate([0, 225, 270]) {
                prism(box_length + 2, 3, 3);
            }
        }
        
        translate([box_length, -1.4 + box_width, 2]) {
            rotate([0, 225, 270]) {
                prism(extension_length +1, 2, 2);
            }
        }

        
        translate([box_length + extension_length - 1.4, 
            (box_width - end_grip_length) - 1.4, 2]) {
            
            rotate([0, 45, 0]) {
                prism(end_grip_length + 2, 2, 2);
            }
        }



    }

}

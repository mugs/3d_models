include <roundedcube.scad>

module card_tray(
	card_size = [68, 44, 32],
	roundedness = 5,
	card_incursion = 2.5, // how far into the rounded corners the card well should go
	bottom_thickness = 2,
	card_buffer = 2, // extra space for cards on X and Y axes
	wall_x = 20, // how far in the walls go on the X axis
	wall_y = 12, // how far in the walls go on the Y axis
	bottom_text = "",
	text_size = 5,
) {

	card_x = card_size[0];
	card_y = card_size[1];
	card_z = card_size[2];

	box_length = card_x + card_buffer + (roundedness * 2) - (card_incursion * 2);
	box_width = card_y + card_buffer + (roundedness * 2) - (card_incursion * 2);
	box_height = card_z + bottom_thickness + (roundedness * 2);

	translate([0, 0, 0 - roundedness]) {
		difference() {

			union() {
				roundedcube([box_length, box_width, box_height], false, roundedness);


			}
	
			union() {
		
				cube([box_length, box_width, roundedness]);
		
				translate([0, 0, box_height - roundedness]) {
					cube([box_length, box_width, roundedness + 1]);
				}
				
				translate([0, 0, roundedness]) {
					translate([roundedness - card_incursion, roundedness - card_incursion, bottom_thickness]) {
						cube([card_x + card_buffer, card_y + card_buffer, box_height]);
					}
					translate([wall_x, 0, bottom_thickness]) {
						cube([box_length - (wall_x *2), box_width, box_height]);
					}
					translate([0, wall_y, bottom_thickness]) {
						cube([box_length, box_width - (wall_y * 2), box_height]);
					}
				}
		
				echo("Box_length = ", box_length);
				echo("box_width = ", box_width);
				echo("height", roundedness + 0.95)

				translate([box_length / 2, box_width / 2, roundedness + 0.75]) {
					rotate([180, 0, 180]) {
						linear_extrude(2) {
							text(bottom_text, text_size, halign="center");
						}
					}
				}
			}

		}
	}
}
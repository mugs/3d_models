include <roundedcube.scad>

module nesting_bin_lid_3_well(
	lid_size = [100, 100, 100],
	lid_wall_thickness = 1.75,
	lid_top_thickness = 2,
	bottom_thickness = 1.2, // thickness of the bottom of the bin wells
	slide_buffer = 0.5,
	finger_hole_radius = 10,
	bin_outer_wall_thickness = 2, // thickness of the outside walls of the bin; not used for lid
	bin_inner_wall_thickness = 2, // thickness of the walls between the bin wells; not used for lid
	bin_outside_roundedness = 5, // radius of the rounded corners of the BIN that will fit inside the lid
	bin_inside_roundedness = 7, // radius of the rounded corners of the bin wells; not used for lid
	compartment1_x = 10,
	compartment2_x = 10,
	bottom_text = "",
	text_size = 5,
) {
	
	box_x = lid_size[0] - (lid_wall_thickness * 2) - slide_buffer;
	box_y = lid_size[1] - (lid_wall_thickness * 2) - slide_buffer;
	box_z = lid_size[2] - lid_top_thickness;

	total_box_z = box_z + (bin_outside_roundedness * 2);
	lid_outside_roundedness = bin_outside_roundedness + lid_wall_thickness; 

	outer_box_x = lid_size[0];
	outer_box_y = lid_size[1];
	outer_box_z = lid_size[2];

	total_outer_box_z = outer_box_z + (lid_outside_roundedness * 2);

	difference() {

		union() {
			translate([0, 0, 0 - lid_outside_roundedness]) {
				difference() {
					union() {
						roundedcube([outer_box_x, outer_box_y, total_outer_box_z], false, lid_outside_roundedness);
					}
					
					//Remove the rounded top and bottom of the rounded cube to leave a rounded rectangle
					union() {
						cube([outer_box_x, outer_box_y, lid_outside_roundedness]);
					
						translate([0, 0, total_outer_box_z - lid_outside_roundedness]) {
							cube([outer_box_x, outer_box_y, lid_outside_roundedness]);
						}
					}
				}
			}
		}

		union () {
			// Subtract a rounded rectangle the size of the inner box (plus the slide buffer)
			// the bin_outside_roundedness variable in this section refers to the outside of the INNER box
			translate([lid_wall_thickness, lid_wall_thickness, lid_top_thickness - bin_outside_roundedness]) {
				difference() {

					hole_x = box_x + slide_buffer;
					hole_y = box_y + slide_buffer;

					echo ("Hole_x: ", hole_x);
					echo ("Hole_y: ", hole_y);
					echo ("bin_outside_roundedness: ", bin_outside_roundedness);

					union() {
						roundedcube([hole_x, hole_y, total_box_z], false, bin_outside_roundedness);
					}
				
					// subtract the bottom and top rounded portions to turn rounded cube
					// into rounded rectangle
					union() {
					
						cube([hole_x, hole_y, bin_outside_roundedness]);
					
						translate([0, 0, box_z + bin_outside_roundedness]) {
							cube([hole_x, hole_y, bin_outside_roundedness]);
						}
					}
				}               
			}
			
			//Finger holes
			translate([outer_box_x / 2, outer_box_y / 2, outer_box_z]) {
				rotate([90, 0, 0]) {
					cylinder(h = outer_box_y + 2, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
				}
			}

			
			// Add text to the bottom of the lid
			translate([outer_box_x / 2, outer_box_y / 2, 0.95]) {
				rotate([180, 0, 180]) {
					linear_extrude(1) {
						text(bottom_text, text_size, halign="center");
					}
				}
			}
		}
	}               
}
include <roundedcube.scad>
//include <prism.scad>


module card_box(
	box_size = [100, 100, 20],
	card_buffer = [1, 1, 1],
	slide_buffer = 0.4,
	text_line_1 = "",
	text_line_2 = "",
	bottom_text_size = 5,
	front_text_size = 5,
	side_text_size = 5,
	front_text_inset = 2,
	box_side_thickness = 3.0,
	box_bottom_thickness = 1.4,
	lip_thickness = 1.6,
	lip_height = 25,
	outer_box_roundedness = 3, 
	inner_box_roundedness = 2,
	z_seam_slot_width = 3,
	z_seam_slot_depth = 0.5,
	finger_slot_radius = 13
) {

	card_width = box_size[0];
	card_height = box_size[1];
	card_thickness = box_size[2];

	card_buffer_x = card_buffer[0];
	card_buffer_y = card_buffer[2];
	card_buffer_z = card_buffer[1];



	outer_box_total_x = card_width + (box_side_thickness * 2) + card_buffer_x;
	outer_box_total_y = card_thickness + (box_side_thickness * 2) + card_buffer_y;
	outer_box_total_z = card_height + box_bottom_thickness + card_buffer_z - lip_height;

	inner_box_total_x = card_width + (lip_thickness * 2) + card_buffer_x;
	inner_box_total_y = card_thickness + (lip_thickness * 2) + card_buffer_y;
	inner_box_total_z = card_height + box_bottom_thickness + card_buffer_z;

	difference() {
		union() {
		
			//outer box
			translate([0, 0, 0 - outer_box_roundedness]) { 
				difference() { 
					roundedcube([outer_box_total_x, outer_box_total_y, outer_box_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
			
				
					union() {
						cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
						translate([0, 0, outer_box_total_z + outer_box_roundedness]) {
							cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
						}
					}
				
				}
			}
		
			//inner box
			translate([box_side_thickness - lip_thickness, box_side_thickness - lip_thickness, 0 - inner_box_roundedness]) { 
				difference() { 
					roundedcube([inner_box_total_x, inner_box_total_y, inner_box_total_z +  (inner_box_roundedness * 2)], false, inner_box_roundedness);
			
				
					union() {
						cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
						translate([0, 0, inner_box_total_z + inner_box_roundedness]) {
							cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
						}
					}
				
				}
			}	
		}

		union(){
			//card cavity
			translate([box_side_thickness, box_side_thickness, box_bottom_thickness]) {
				cube([card_width + card_buffer_x, card_thickness + card_buffer_y, card_height + 100]);
			}

			//z-seam slot
			translate([
					box_side_thickness - lip_thickness, 
					(outer_box_total_y - z_seam_slot_width) / 2,
					outer_box_total_z
				]) {
			
				cube([z_seam_slot_depth, z_seam_slot_width, 100]);
				
			}

			// finger slot
			translate([outer_box_total_x / 2, 0, inner_box_total_z]) {
				rotate([-90, 0, 0]) {
					cylinder(h=outer_box_total_y,r=finger_slot_radius);
				}
			}


			// bottom text
			translate([outer_box_total_x / 2, outer_box_total_y / 2, 0.95]) {
				rotate([180, 0, 180]) {
					linear_extrude(1) {
						text(text_line_1, bottom_text_size, halign="center");
					}
				}
			}

			// front text
			translate([outer_box_roundedness + front_text_inset + front_text_size, 0.95, outer_box_total_z / 2]) {
				rotate([0, -90, 90]) {
					linear_extrude(1) {
						text(text_line_1, front_text_size, halign="center");
					}
				}
			}
		
			// side text
			translate([0.95, outer_box_total_y / 2, outer_box_total_z / 2]) {
				rotate([-90, -90, 90]) {
					linear_extrude(1) {
						text(text_line_1, front_text_size, halign="center");
					}
				}
			}


		}

	}


   
}
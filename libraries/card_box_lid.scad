include <roundedcube.scad>
//include <prism.scad>


module card_box_lid(
	box_size = [100, 100, 20],
	card_buffer = [1, 1, 1],
	slide_buffer = 0.4,
	text_line_1 = "",
	text_line_2 = "",
	bottom_text_size = 5,
	front_text_size = 5,
	side_text_size = 5,
	front_text_inset = 2,
	box_side_thickness = 3.0,
	box_bottom_thickness = 1.4,
	lip_thickness = 1.6,
	lip_height = 25,
	outer_box_roundedness = 3, 
	inner_box_roundedness = 2, 
	cavity_roundedness = 2,
	z_seam_slot_width = 3,
	z_seam_slot_depth = 0.5,
	finger_slot_radius = 13
) {

	card_width = box_size[0];
	card_height = box_size[1];
	card_thickness = box_size[2];

	card_buffer_x = card_buffer[0];
	card_buffer_y = card_buffer[2];
	card_buffer_z = card_buffer[1];

	lid_total_x = card_width + (box_side_thickness * 2) + card_buffer_x;
	lid_total_y = card_thickness + (box_side_thickness * 2) + card_buffer_y;
	lid_total_z = lip_height + box_bottom_thickness;

	cavity_total_x = card_width + (lip_thickness * 2) + card_buffer_x + slide_buffer;
	cavity_total_y = card_thickness + (lip_thickness * 2) + card_buffer_y + slide_buffer;


	difference() {
		union() {
		
			//outer box
			translate([0, 0, 0 - outer_box_roundedness]) { 
				difference() { 
					roundedcube([lid_total_x, lid_total_y, lid_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
			
				
					union() {
						cube([lid_total_x, lid_total_y, outer_box_roundedness]);
						translate([0, 0, lid_total_z + outer_box_roundedness]) {
							cube([lid_total_x, lid_total_y, outer_box_roundedness]);
						}
					}
				
				}
			}        
		}

		union(){
	
			//cavity 
			translate([
				box_side_thickness - lip_thickness - (slide_buffer / 2), 
				box_side_thickness - lip_thickness - (slide_buffer / 2), 
				0 - cavity_roundedness + box_bottom_thickness
			]) { 
				difference() { 
					roundedcube([cavity_total_x, cavity_total_y, 1000], false, cavity_roundedness);
			
				
					union() {
						cube([cavity_total_x, cavity_total_y, cavity_roundedness]);
					}
				
				}
			}


			//z-seam slot
			translate([
					box_side_thickness - lip_thickness - z_seam_slot_depth, 
					(lid_total_y - z_seam_slot_width) / 2,
					box_bottom_thickness
				]) {
			
				cube([z_seam_slot_depth, z_seam_slot_width, 100]);
				
			}


			// bottom text
			translate([lid_total_x / 2, lid_total_y / 2, 0.95]) {
				rotate([180, 0, 180]) {
					linear_extrude(1) {
						text(text_line_1, bottom_text_size, halign="center");
					}
				}
			}
		}

	}    
}
include <prism.scad>
include <roundedcube.scad>

module tab_lid( 
	box_size = [100, 100, 100],
	side_wall_thickness = 3,
	inner_wall_thickness = 2,
	bottom_thickness = 2,
	roundedness = 7,
	compartments = [25, 25, 25],
	closer_cube = [10, 10, 4],
	slider_height = 3,
	sphere_radius = 12,
	sphere_protrusion = 0.8,
	gap = 0.5,
	lid_height = 1.6
) {

	box_length = box_size[0];
	box_width = box_size[1];
	box_height = box_size[2];

	sphere_radius = 8;
	sphere_protrusion = 1;
	gap = 0.5;

	lid_length = box_length - inner_wall_thickness;
	lid_width = box_width - (2*side_wall_thickness) - (gap) + 1;

	difference() {
		union() {
			cube([lid_length, lid_width, lid_height]);
			translate([0, 0, 0]) {
				rotate([0, 0, 270]) {
					prism(box_length - inner_wall_thickness, 0.6, lid_height);
				}
			}
			translate([lid_length, lid_width, 0]) {
				rotate([0, 0, 90]) {
					prism(box_length - inner_wall_thickness, 0.6, lid_height);
				}
			}
		}

		union() {
			translate([box_length - inner_wall_thickness - (closer_cube_x/2) + 0.15, lid_width / 2, 0 - sphere_radius + sphere_protrusion + 0]) {
				sphere(sphere_radius);
			}

			translate([lid_length, lid_width + 3, 0]) {
				rotate([0, 0, 180]) {
					prism(box_length, 1.5, 1.9);
				}
			}
			difference() {
				translate([8, lid_width / 2, 16 + lid_height - 1.2]) {
					sphere(16);
				}
				translate([-8, lid_width / 2 - 8, lid_height - 8]) {
					cube([16, 16, 16]);
				}
		   }
	   }
	}        
}
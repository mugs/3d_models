include <roundedcube.scad>
include <prism.scad>


module tab_lid_bin(
	box_size = [100, 100, 100],
	side_wall_thickness = 3,
	inner_wall_thickness = 2,
	bottom_thickness = 2,
	roundedness = 7,
	compartments = [25, 25, 25],
	closer_cube = [10, 10, 4],
	slider_height = 3,
	sphere_radius = 12,
	sphere_protrusion = 0.8
) {



	box_length = box_size[0];
	box_width = box_size[1];
	box_height = box_size[2];

	//side_wall_thickness = 3;

	//inner_wall_thickness = 2;
	//bottom_thickness = 2;
	//roundedness = 7;
	compartment1_x = compartments[0];
	compartment2_x = compartments[1];
	compartment3_x = box_length - compartment1_x - compartment2_x - (inner_wall_thickness * 4);

	closer_cube_x = closer_cube[0];
	closer_cube_y = closer_cube[1];
	closer_cube_z = closer_cube[2];

	//slider_height = 3;

	//sphere_radius = 12;
	//sphere_protrusion = 0.8;

	union() {
		difference() {
			union() {
				cube([box_length, box_width, box_height]);
			}
			union() {
				translate([0, side_wall_thickness, box_height - slider_height]) {
					cube([box_length - inner_wall_thickness, box_width - (side_wall_thickness * 2), slider_height]);
				}

				translate([inner_wall_thickness, side_wall_thickness, bottom_thickness]) {
					roundedcube([compartment1_x, box_width - (side_wall_thickness * 2), 
						box_height + roundedness], false, roundedness);
				}
				translate([inner_wall_thickness + compartment1_x + inner_wall_thickness, side_wall_thickness, bottom_thickness]) {
					roundedcube([compartment2_x, box_width - (side_wall_thickness * 2), 
						box_height + roundedness], false, roundedness);
				}
				translate([(inner_wall_thickness * 3) + compartment1_x + compartment2_x, side_wall_thickness, bottom_thickness]) {
					roundedcube([compartment3_x, box_width - (side_wall_thickness * 2), 
						box_height + roundedness], false, roundedness);
				}

				//lid
				translate([0, side_wall_thickness, box_height - slider_height]) {
					cube([box_length - inner_wall_thickness, box_width - (side_wall_thickness * 2), slider_height]);
				}

				translate([0 + box_length - inner_wall_thickness, box_width - side_wall_thickness - .0001, box_height - slider_height]) {
					rotate([0, 0, 90]) {
						prism(box_length + inner_wall_thickness, 1.2, slider_height);
					}
				}
			
				translate([0 - inner_wall_thickness, side_wall_thickness + .0001, box_height - slider_height]) {
					rotate([0, 0, 270]) {
						prism(box_length, 1.2, slider_height);
					}
				}
			}
		}

		start_x = box_length - closer_cube_x - inner_wall_thickness;
		start_y = (box_width / 2) - (closer_cube_y / 2);
		back_wall_y = (box_width / 2);
		start_z = box_height - slider_height - closer_cube_z;
		translate([start_x, start_y, start_z]) {
			cube([closer_cube_x, closer_cube_y, closer_cube_z]);
		}
		
		points = [
			//right side
			[start_x, start_y, start_z], // 0
			[start_x + closer_cube_x, start_y, start_z], // 1
			[start_x + closer_cube_x, back_wall_y - 1, start_z - closer_cube_x], // 2
		
			// left side
			[start_x, start_y + closer_cube_y, start_z], // 3
			[start_x + closer_cube_x, start_y + closer_cube_y, start_z], // 4
			[start_x + closer_cube_x, back_wall_y + 1, start_z - closer_cube_x], // 5
		];

		faces = [
			[0, 1, 2], // right side
			[5, 4, 3], //left side
			[3, 0, 2, 5], //bottom
			[1, 4, 5, 2], //back
			[0, 3, 4, 1], //top

		];

		polyhedron(points, faces);
	
		difference() {
			translate([box_length - inner_wall_thickness - (closer_cube_x/2), box_width / 2, box_height - slider_height - sphere_radius + sphere_protrusion]) {
				sphere(sphere_radius);
			}
		
			translate([0, 0, 0 - slider_height - 0.1]) {
				cube([box_length * 2, box_width, box_height]);
			}
		}
	}
}

module tab_lid( 
	box_size = [100, 100, 100],
	side_wall_thickness = 3,
	inner_wall_thickness = 2,
	bottom_thickness = 2,
	roundedness = 7,
	compartments = [25, 25, 25],
	closer_cube = [10, 10, 4],
	slider_height = 3,
	sphere_radius = 12,
	sphere_protrusion = 0.8,
	gap = 0.5,
	lid_height = 1.6
) {

	box_length = box_size[0];
	box_width = box_size[1];
	box_height = box_size[2];

	//sphere_radius = 8;
	//sphere_protrusion = 1;
	//gap = 0.5;

	closer_cube_x = closer_cube[0];
	closer_cube_y = closer_cube[1];
	closer_cube_z = closer_cube[2];


	lid_length = box_length - inner_wall_thickness;
	lid_width = box_width - (2*side_wall_thickness) - (gap) + 1;

	difference() {
		union() {
			cube([lid_length, lid_width, lid_height]);
			translate([0, 0, 0]) {
				rotate([0, 0, 270]) {
					prism(box_length - inner_wall_thickness, 0.6, lid_height);
				}
			}
			translate([lid_length, lid_width, 0]) {
				rotate([0, 0, 90]) {
					prism(box_length - inner_wall_thickness, 0.6, lid_height);
				}
			}
		}

		union() {
			translate([box_length - inner_wall_thickness - (closer_cube_x/2) + 0.15, lid_width / 2, 0 - sphere_radius + sphere_protrusion + 0]) {
				sphere(sphere_radius);
			}

			translate([lid_length, lid_width + 3, 0]) {
				rotate([0, 0, 180]) {
					prism(box_length, 1.5, 1.9);
				}
			}
			difference() {
				translate([8, lid_width / 2, 16 + lid_height - 1.2]) {
					sphere(16);
				}
				translate([-8, lid_width / 2 - 8, lid_height - 8]) {
					cube([16, 16, 16]);
				}
		   }
	   }
	}        
}
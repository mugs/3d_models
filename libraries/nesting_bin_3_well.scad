include <roundedcube.scad>

module nesting_bin_3_well(
	lid_size = [100, 100, 100],
	lid_wall_thickness = 1.75,
	lid_top_thickness = 2,
	bottom_thickness = 1.2, // thickness of the bottom of the bin wells
	slide_buffer = 0.5,
	bin_outer_wall_thickness = 2, // thickness of the outside walls of the bin; not used for lid
	bin_inner_wall_thickness = 2, // thickness of the walls between the bin wells; not used for lid
	bin_outside_roundedness = 5, // radius of the rounded corners of the BIN that will fit inside the lid
	bin_inside_roundedness = 7, // radius of the rounded corners of the bin wells; not used for lid
	compartment1_x = 10,
	compartment2_x = 10,
	bottom_text = "",
	text_size = 5,
	skip_compartments = false
) {
	
	box_x = lid_size[0] - (lid_wall_thickness * 2) - slide_buffer;
	box_y = lid_size[1] - (lid_wall_thickness * 2) - slide_buffer;
	box_z = lid_size[2] - lid_top_thickness;

	total_box_z = box_z + (bin_outside_roundedness * 2);
	lid_outside_roundedness = bin_outside_roundedness + lid_wall_thickness; 

	outer_box_x = lid_size[0];
	outer_box_y = lid_size[1];
	outer_box_z = lid_size[2];

	total_outer_box_z = outer_box_z + (lid_outside_roundedness * 2);

	difference() {

		union() {
			translate([0, 0, 0 - bin_outside_roundedness]) {
				difference() {
					union() {
						roundedcube([box_x, box_y, total_box_z], false, bin_outside_roundedness);
					}
				
					union() {
						cube([box_x, box_y, bin_outside_roundedness]);

						translate([0, 0, box_z + bin_outside_roundedness]) {
							cube([box_x, box_y, bin_outside_roundedness]);
						}
					}
				}
			}
		}

		union ()
		{
			if(!skip_compartments) {
				//compartment 1
				translate([bin_outer_wall_thickness, bin_outer_wall_thickness, bottom_thickness]) {
					roundedcube([compartment1_x, box_y - (bin_outer_wall_thickness * 2), 
						box_z + bin_inside_roundedness], false, bin_inside_roundedness);
				}
			
				//compartment 2
				translate([bin_outer_wall_thickness + compartment1_x + bin_inner_wall_thickness, bin_outer_wall_thickness, bottom_thickness]) {
					roundedcube([compartment2_x, box_y - (bin_outer_wall_thickness * 2), 
						box_z + bin_inside_roundedness], false, bin_inside_roundedness);
				}
			
				//compartment 3
				compartment3_x = box_x - ((bin_outer_wall_thickness * 2) + compartment1_x + (2 * bin_inner_wall_thickness) + compartment2_x);
				translate([bin_outer_wall_thickness + compartment1_x + bin_inner_wall_thickness + compartment2_x + bin_inner_wall_thickness, bin_outer_wall_thickness, bottom_thickness]) {
					roundedcube([compartment3_x, box_y - (bin_outer_wall_thickness * 2), 
						box_z + bin_inside_roundedness], false, bin_inside_roundedness);
				}
			}

			// Add text to the bottom of the lid
			translate([box_x / 2, box_y / 2, 0.95]) {
				rotate([180, 0, 180]) {
					linear_extrude(1) {
						text(bottom_text, text_size, halign="center");
					}
				}
			}
		}
	}
}
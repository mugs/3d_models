include <roundedcube.scad>


$fn = $preview ? 12 : 72;

game_title = "Welcome To";
bottom_text_size = 5;
front_text_size = 5;
front_text_inset = 2;
side_text_size = 5;

card_width = 58;
card_height = 88;
card_thickness = 30;

card_buffer_x = 1;
card_buffer_y = 1;
card_buffer_z = 1;


box_side_thickness = 2.4;
box_bottom_thickness = 1.4;
lip_thickness = 1;
lip_height = 25;

outer_box_roundedness = 3;
inner_box_roundedness = 2;

outer_box_total_x = card_width + (box_side_thickness * 2) + card_buffer_x;
outer_box_total_y = card_thickness + (box_side_thickness * 2) + card_buffer_y;
outer_box_total_z = card_height + box_bottom_thickness + card_buffer_z - lip_height;

inner_box_total_x = card_width + (lip_thickness * 2) + card_buffer_x;
inner_box_total_y = card_thickness + (lip_thickness * 2) + card_buffer_y;
inner_box_total_z = card_height + box_bottom_thickness + card_buffer_z;

z_seam_slot_width = 3;
z_seam_slot_depth = 0.5;

finger_slot_radius = 10;

difference() {
	union() {
		
        //outer box
        translate([0, 0, 0 - outer_box_roundedness]) { 
            difference() { 
                roundedcube([outer_box_total_x, outer_box_total_y, outer_box_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
            
                
                union() {
                    cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    translate([0, 0, outer_box_total_z + outer_box_roundedness]) {
                        cube([outer_box_total_x, outer_box_total_y, outer_box_roundedness]);
                    }
                }
                
            }
        }
        
        //inner box
        translate([box_side_thickness - lip_thickness, box_side_thickness - lip_thickness, 0 - inner_box_roundedness]) { 
            difference() { 
                roundedcube([inner_box_total_x, inner_box_total_y, inner_box_total_z +  (inner_box_roundedness * 2)], false, inner_box_roundedness);
            
                
                union() {
                    cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
                    translate([0, 0, inner_box_total_z + inner_box_roundedness]) {
                        cube([inner_box_total_x, inner_box_total_y, inner_box_roundedness]);
                    }
                }
                
            }
        }	
    }

	union(){
        //card cavity
        translate([box_side_thickness, box_side_thickness, box_bottom_thickness]) {
            cube([card_width + card_buffer_x, card_thickness + card_buffer_y, card_height + 100]);
        }

        //z-seam slot
        translate([
                box_side_thickness - lip_thickness, 
                (outer_box_total_y - z_seam_slot_width) / 2,
                outer_box_total_z
            ]) {
            
            cube([z_seam_slot_depth, z_seam_slot_width, 100]);
                
        }

        // finger slot
        translate([outer_box_total_x / 2, 0, inner_box_total_z]) {
            rotate([-90, 0, 0]) {
                cylinder(h=outer_box_total_y,r=finger_slot_radius);
            }
        }


		// bottom text
		translate([outer_box_total_x / 2, outer_box_total_y / 2, 0.95]) {
			rotate([180, 0, 180]) {
				linear_extrude(1) {
					text(game_title, bottom_text_size, halign="center");
				}
			}
		}

		// front text
		translate([outer_box_roundedness + front_text_inset + front_text_size, 0.95, outer_box_total_z / 2]) {
			rotate([0, -90, 90]) {
				linear_extrude(1) {
					text(game_title, front_text_size, halign="center");
				}
			}
		}
        
		// side text
		translate([0.95, outer_box_total_y / 2, outer_box_total_z / 2]) {
			rotate([-90, -90, 90]) {
				linear_extrude(1) {
					text(game_title, front_text_size, halign="center");
				}
			}
		}


	}

}
include <nesting_bin_3_well.scad>
 
 difference() {
        
     nesting_bin_3_well(
        lid_size = [110, 130, 42],
        bottom_text = "Bits Bits Bits",
        text_size = 5,
        bin_outer_wall_thickness = 1.5,
        bin_inner_wall_thickness = 1,
        skip_compartments = true,
        slide_buffer = 0.75,
        lid_wall_thickness = 1.5
    );

    translate([1.5, 1.5, 1.5]) {
        roundedcube([103.5, 123.5, 300], false, 3);
    }
};
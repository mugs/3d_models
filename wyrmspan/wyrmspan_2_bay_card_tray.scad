include <roundedcube.scad>

line1_text = "Birds Birds Birds";
line2_text = "Dragons";
text_size = 5;
text_size_2 = 12;
card_x = 59;
card_y = 93;
card_z = 39;
card_incursion = 2.5; // how far into the rounded corners the card well should go
bottom_thickness = 2;
roundedness = 5;
card_buffer = 2;
inner_wall_thickness = 2;

wall_thickness = 2;
top_thickness = 2;
outer_roundedness = roundedness + wall_thickness; 
slide_buffer = 0.5;


wall_x = 17;
wall_y = 27;

box_length = (card_x * 2) + (card_buffer * 2) + (roundedness * 2) - (card_incursion * 2) + inner_wall_thickness;
box_width = card_y + card_buffer + (roundedness * 2) - (card_incursion * 2);
box_height = card_z + bottom_thickness + (roundedness * 2);
x_opening_width = card_x + roundedness - (wall_x * 2);


difference() {

    union() {
        roundedcube([box_length, box_width, box_height], false, roundedness);
    }
    
    union() {
        
        // Remove bottom of rounded cube to form rounded rectangle
        cube([box_length, box_width, roundedness]);
        
        // Remove top of rounded cube to form rounded rectangle
        translate([0, 0, box_height - roundedness]) {
            cube([box_length, box_width, roundedness + 1]);
        }
        
        //Card well 1
        translate([0, 0, roundedness]) {
            translate([roundedness - card_incursion, roundedness - card_incursion, bottom_thickness]) {
                cube([card_x + card_buffer, card_y + card_buffer, box_height]);
            }
            
            translate([wall_x, 0, bottom_thickness]) {
                cube([x_opening_width, box_width, box_height]);
            }            
            translate([0, wall_y, bottom_thickness]) {
                cube([box_length, box_width - (wall_y * 2), box_height]);
            }
           
        }
        
        //Card well 2
        well_2_x = box_length - roundedness + card_incursion - card_x - card_buffer;
        translate([well_2_x, 0, roundedness]) {
            translate([0, roundedness - card_incursion, bottom_thickness]) {
                cube([card_x + card_buffer, card_y + card_buffer, box_height]);
            }
            
            translate([wall_x, 0, bottom_thickness]) {
                cube([x_opening_width, box_width, box_height]);
            }            
            translate([0, wall_y, bottom_thickness]) {
                cube([box_length, box_width - (wall_y * 2), box_height]);
            }
           
        }
        
        translate([box_length / 2, box_width / 2 + 10, roundedness + 0.95]) {
            rotate([180, 0, 180]) {
                linear_extrude(1) {
                    text(line1_text, text_size, halign="center");
                }
            }
        }
        
        //janky strike-through
        translate([39.5, box_width / 2 + 12, roundedness - 9.05]) {
            cube([50, 1, 10]);
            
        }

        
        translate([box_length / 2, (box_width / 2) - 10, roundedness + 0.95]) {
            rotate([180, 0, 180]) {
                linear_extrude(1) {
                    text(line2_text, text_size_2, halign="center");
                }
            }
        }
        
    }

}               

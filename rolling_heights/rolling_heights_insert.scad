
lid_total_z = 4;
tile_height = 50;
bottom_thickness = 1;

total_x = 160;
total_y = 130;
total_z = lid_total_z + tile_height + bottom_thickness;

vertical_empty_space = 15;

finger_hold_size = 10;
side_thickness = 3;


lv_1_x = 90;
lv_1_y = 51;

lv_2_x = 76;
lv_2_y = 51;

tile_cavity_x_buffer = 10;
tile_cavity_y_buffer = 10;

tile_cavity_middle_buffer = total_y - ((2*side_thickness) + lv_1_y + lv_2_y + (2*tile_cavity_y_buffer));

echo(tile_cavity_middle_buffer);

difference() {

	union() {
		cube([total_x, total_y, total_z]);
	}
	
	union() {
		translate([side_thickness, side_thickness, tile_height + bottom_thickness - vertical_empty_space]) {
			cube([total_x - (side_thickness * 2), total_y - (side_thickness * 2), total_z]);
		
        }
        
		translate([side_thickness + tile_cavity_x_buffer, side_thickness + tile_cavity_y_buffer, bottom_thickness]) {
			cube([lv_1_x, lv_1_y, total_z]);
		}

		translate([side_thickness + tile_cavity_x_buffer, side_thickness + tile_cavity_y_buffer + lv_1_y + tile_cavity_middle_buffer, bottom_thickness]) {
			cube([lv_2_x, lv_2_y, total_z]);
		}

        
	}
}




/*
extra_side_buffer = 1;

square_size = 10;
square_buffer = 1;


tower_width = 10;
cube_inside_width = 5.95;
cube_inside_height = 8;
pillar_offset = ((tower_width - cube_inside_width) / 2);

base_roundedness = 1;
pillar_roundness = 1.5;

for(i = [0:2]) {
	for(j = [0:2]) {
		if(layout[i][j] == 1) {
            translate([(i * square_size) - square_buffer, (j * square_size) - square_buffer, 0]) {
                roundedcube([square_size + square_buffer * 2, square_size + square_buffer * 2, bottom_thickness], false, base_roundedness);
            }

		    translate([(i * square_size) + pillar_offset, (j * square_size) + pillar_offset, 0]) {
		       roundedcube([cube_inside_width, cube_inside_width, cube_inside_height + bottom_thickness], false, pillar_roundness);
		    }

        }
	}
}



translate([(0.5 * square_size) - square_buffer, (1 * square_size) - square_buffer, 0]) {
	roundedcube([square_size + square_buffer * 2, square_size + square_buffer * 2, bottom_thickness], false, base_roundedness);
}

translate([(0.5 * square_size) + pillar_offset, (1 * square_size) + pillar_offset, 0]) {
   roundedcube([cube_inside_width, cube_inside_width, cube_inside_height + bottom_thickness], false, pillar_roundness);
}

*/
include <roundedcube.scad>


layout = [
    [1, 0, 1],
    [1, 0, 1],
    [0, 0, 0]
];


bottom_thickness = 2;
extra_side_buffer = 1;

square_size = 10;
square_buffer = 1;


tower_width = 10;
cube_inside_width = 5.95;
cube_inside_height = 8;
pillar_offset = ((tower_width - cube_inside_width) / 2);

base_roundedness = 1;
pillar_roundness = 1.5;

for(i = [0:2]) {
	for(j = [0:2]) {
		if(layout[i][j] == 1) {
            translate([(i * square_size) - square_buffer, (j * square_size) - square_buffer, 0]) {
                roundedcube([square_size + square_buffer * 2, square_size + square_buffer * 2, bottom_thickness], false, base_roundedness);
            }

		    translate([(i * square_size) + pillar_offset, (j * square_size) + pillar_offset, 0]) {
		       roundedcube([cube_inside_width, cube_inside_width, cube_inside_height + bottom_thickness], false, pillar_roundness);
		    }

        }
	}
}



translate([(0.5 * square_size) - square_buffer, (1 * square_size) - square_buffer, 0]) {
	roundedcube([square_size + square_buffer * 2, square_size + square_buffer * 2, bottom_thickness], false, base_roundedness);
}

translate([(0.5 * square_size) + pillar_offset, (1 * square_size) + pillar_offset, 0]) {
   roundedcube([cube_inside_width, cube_inside_width, cube_inside_height + bottom_thickness], false, pillar_roundness);
}


tile_height = 50;
bottom_thickness = 1;

total_x = 131.5;
total_y = 91.5;
total_z = tile_height + bottom_thickness;
main_compartment_z = total_z - 14;

corner_pillar_size = 7;


lv_1_x_corner = 2;
lv_1_y_corner = corner_pillar_size + 1;
lv_1_x_size = 91;
lv_1_y_size = 51;

ad_x_corner = lv_1_x_corner + lv_1_x_size + 9;
ad_y_corner = lv_1_y_corner;
ad_z_corner = tile_height - 27 + bottom_thickness;
ad_x_size = 28;
ad_y_size = 51;

wild_x_corner = 18;
wild_y_corner = lv_1_y_corner + lv_1_y_size + 2;
wild_z_corner = tile_height - 27 + bottom_thickness;
wild_x_size = 65;
wild_y_size = 28;

solo_x_corner = wild_x_corner + wild_x_size + 18;
solo_y_corner = wild_y_corner;
solo_z_corner = wild_z_corner;
solo_x_size = 13.5;
solo_y_size = 28;

difference() {

	union() {
		cube([total_x, total_y, main_compartment_z]);
		cube([corner_pillar_size, corner_pillar_size, total_z]);
		translate([total_x - corner_pillar_size, 0, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
		translate([0, total_y - corner_pillar_size, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
		translate([total_x - corner_pillar_size, total_y - corner_pillar_size, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
	}
	
	union() {
		translate([lv_1_x_corner, lv_1_y_corner, bottom_thickness]) {
			cube([lv_1_x_size, lv_1_y_size, total_z]);
		
        }
		translate([ad_x_corner, ad_y_corner, ad_z_corner]) {
			cube([ad_x_size, ad_y_size, total_z]);
		
        }
		translate([wild_x_corner, wild_y_corner, wild_z_corner]) {
			cube([wild_x_size, wild_y_size, total_z]);
		
        }
		translate([solo_x_corner, solo_y_corner, solo_z_corner]) {
			cube([solo_x_size, solo_y_size, total_z]);
		
        }
        
	}
}

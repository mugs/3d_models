
tile_height = 50;
bottom_thickness = 1;

total_x = 131.5;
total_y = 91.5;
total_z = tile_height + bottom_thickness;
main_compartment_z = total_z - 14;

corner_pillar_size = 7;


lv_2_x_corner = 5;
lv_2_y_corner = 20;
lv_2_x_size = 76.5;
lv_2_y_size = 51;

target_x_corner = lv_2_x_corner + lv_2_x_size + 16;
target_y_corner = lv_2_y_corner - 5;
target_x_size = 29;
target_y_size = 60;


difference() {

	union() {
		cube([total_x, total_y, main_compartment_z]);
		cube([corner_pillar_size, corner_pillar_size, total_z]);
		translate([total_x - corner_pillar_size, 0, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
		translate([0, total_y - corner_pillar_size, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
		translate([total_x - corner_pillar_size, total_y - corner_pillar_size, 0]) {
			cube([corner_pillar_size, corner_pillar_size, total_z]);
		}
	}
	
	union() {
		translate([lv_2_x_corner, lv_2_y_corner, bottom_thickness]) {
			cube([lv_2_x_size, lv_2_y_size, total_z]);
        }
		translate([target_x_corner, target_y_corner, bottom_thickness]) {
			cube([target_x_size, target_y_size, total_z]);
        }
        
	}
}

include <nesting_bin_lid_3_well.scad>
 
       
 nesting_bin_lid_3_well(
    lid_size = [57, 35, 20],
    bottom_text = "Trains",
    text_size = 5,
    bin_outer_wall_thickness = 1.5,
    bin_inner_wall_thickness = 1,
    slide_buffer = 0.75,
    lid_wall_thickness = 1.5
);


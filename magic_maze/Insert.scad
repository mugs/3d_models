

x_size = 235;
y_size = 235;
z_size = 60;

wall_thickness = 2;
floor_thickness = 2;
tile_size = 97;
tile_hole_size = 60;
tile_hole_offset = (tile_size / 2) - (tile_hole_size / 2);
tile_wall_extra_thickness = 3;

card_x = 70;

big_card_y = 20;
small_card_y = 17;

big_card_z = 54;
small_card_z = 43;

floor_inset = 15;

tile_finger_hole_size = (x_size - (wall_thickness * 2) - (tile_wall_extra_thickness * 2) - (tile_size * 2) + 18);


difference() {
    cube([x_size, y_size, z_size]);
    
    
    union() {
        
        // Tile stack 1
        translate([wall_thickness + tile_wall_extra_thickness, wall_thickness, floor_thickness]) {
            cube([tile_size, tile_size, z_size]);
        }
        
        // Tile stack 1 floor hole
        translate([wall_thickness + tile_wall_extra_thickness + tile_hole_offset, wall_thickness + tile_hole_offset, -1]) {
            cube([tile_hole_size, tile_hole_size, z_size]);
        }
        
        
        // Tile stack 2
        x_offset = x_size - wall_thickness - tile_wall_extra_thickness - tile_size;
        translate([x_offset, wall_thickness, floor_thickness]) {
            cube([tile_size, tile_size, z_size]);
        }
        
        // Tile stack 2 floor hole
        translate([x_offset + tile_hole_offset, wall_thickness + tile_hole_offset, -1]) {
            cube([tile_hole_size, tile_hole_size, z_size]);
        }
        
        // Finger hole between tile stacks
        translate([(x_size / 2) - (tile_finger_hole_size / 2), 
                wall_thickness + (tile_size / 2) - (tile_finger_hole_size / 2), 
                -1]) {
            cube([tile_finger_hole_size, tile_finger_hole_size, z_size + 2]);
        }
        
        // Big cavity
        /*        
        translate([wall_thickness + wall_thickness + card_x, 
                    wall_thickness + wall_thickness + tile_size,
                    floor_thickness]) {
            cube([x_size - (wall_thickness + wall_thickness + wall_thickness + card_x),
                  y_size - (wall_thickness + wall_thickness + wall_thickness + tile_size),
                  z_size]);
        }
        
        

        translate([wall_thickness + wall_thickness + card_x + floor_inset, 
                    wall_thickness + wall_thickness + tile_size + floor_inset,
                    -1]) {
            cube([x_size - (wall_thickness + wall_thickness + wall_thickness + card_x + floor_inset * 2),
                  y_size - (wall_thickness + wall_thickness + wall_thickness + tile_size + floor_inset * 2),
                  z_size]);
        }
        */
        translate([wall_thickness + wall_thickness + card_x, 
                    wall_thickness + wall_thickness + tile_size,
                    -1]) {
            cube([x_size - (wall_thickness + wall_thickness + card_x) + 1,
                  y_size - (wall_thickness + wall_thickness + tile_size) + 1,
                  z_size + 2]);
        }
        
        
        
        // Action cards top inset
        translate([wall_thickness, 
                    wall_thickness + wall_thickness + tile_size,
                    45]) {
            cube([card_x,
                  10 + big_card_y + 20 + small_card_y + 10, 
                  z_size]);
                            
        }
        
        // Big card hole
        translate([wall_thickness, 
                    wall_thickness + wall_thickness + tile_size + 10,
                    z_size - big_card_z]) {
            cube([card_x,
                  big_card_y, 
                  z_size]);
        }

        // Small card hole
        translate([wall_thickness, 
                    wall_thickness + wall_thickness + tile_size + 10 + big_card_y + 20,
                    z_size - small_card_z]) {
            cube([card_x,
                  small_card_y, 
                  z_size]);
        }

        // Finger hole between cards
        translate([wall_thickness + 20, 
                    wall_thickness + wall_thickness + tile_size + 10 + big_card_y - 1,
                    z_size - small_card_z]) {
            cube([30,
                  22, 
                  z_size]);
        }
        
        // Bin
        translate([wall_thickness, 
                    wall_thickness + wall_thickness + tile_size + 10 + big_card_y + 20 + small_card_y + 10 + wall_thickness,
                    floor_thickness]) {
            cube([card_x,
                  y_size - (wall_thickness + wall_thickness + tile_size + 10 + big_card_y + 20 + small_card_y + 10 + wall_thickness + wall_thickness), 
                  z_size]);
        }


    }
    
    
}

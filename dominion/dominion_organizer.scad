total_x = 294;
total_y = 291;
total_z = 43;

floor_thickness = 1.6;
outer_wall_thickness = 1.6;
inner_wall_thickness = 1.4;

cavity_width = 96;

divider_thickness = 1.8;

cavity_hole_bottom_offset = 37;
cavity_hole_bottom_height = 20;
cavity_hole_top_offset = 20;
cavity_hole_top_height = 50;

difference() {
    union() {
        cube([total_x, total_y, total_z]);
    }
    
    union() {
        translate([outer_wall_thickness, outer_wall_thickness, floor_thickness]) {
            cube([cavity_width, total_y - (2* outer_wall_thickness), total_z]);
        }
        translate([outer_wall_thickness + cavity_width + inner_wall_thickness, outer_wall_thickness, floor_thickness]) {
            cube([cavity_width, total_y - (2* outer_wall_thickness), total_z]);
        }
        translate([outer_wall_thickness + cavity_width + inner_wall_thickness + cavity_width + inner_wall_thickness, outer_wall_thickness, floor_thickness]) {
            cube([cavity_width, total_y - (2* outer_wall_thickness), total_z]);
        }
    }
}


difference() {
    union() {
        translate([0, 75, 0]) {
            cube([total_x, divider_thickness, total_z]);
        }
        translate([0, 150, 0]) {
            cube([total_x, divider_thickness, total_z]);
        }
        translate([0, 220, 0]) {
            cube([total_x, divider_thickness, total_z]);
        }
        
    }
    
    union() {
        hull() {
            translate([outer_wall_thickness + cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                rotate([90, 0, 0]) {
                    cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                }
            }
            translate([outer_wall_thickness + cavity_width - cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                rotate([90, 0, 0]) {
                    cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                }
            }
            
            translate([outer_wall_thickness + cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                rotate([90, 0, 0]) {
                    cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                }
            }
            translate([outer_wall_thickness + cavity_width - cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                rotate([90, 0, 0]) {
                    cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                }
            }
        }
        
        hull() {
            translate([cavity_width + inner_wall_thickness, 0, 0]) {
                translate([outer_wall_thickness + cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                translate([outer_wall_thickness + cavity_width - cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                
                translate([outer_wall_thickness + cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                translate([outer_wall_thickness + cavity_width - cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
            }
        }
        
        hull() {
            translate([2 * (cavity_width + inner_wall_thickness), 0, 0]) {
                translate([outer_wall_thickness + cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                translate([outer_wall_thickness + cavity_width - cavity_hole_bottom_offset, total_x - outer_wall_thickness, cavity_hole_bottom_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                
                translate([outer_wall_thickness + cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
                translate([outer_wall_thickness + cavity_width - cavity_hole_top_offset, total_x - outer_wall_thickness, cavity_hole_top_height]) {
                    rotate([90, 0, 0]) {
                        cylinder(total_y - (outer_wall_thickness * 2), 5, 5);
                    }
                }
            }
        }
        
    }

}
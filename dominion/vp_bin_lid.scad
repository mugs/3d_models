include <nesting_bin_lid_3_well.scad>
 
 nesting_bin_lid_3_well(
    lid_size = [95, 69, 30],
    bottom_text = "Victory Points",
    text_size = 5,
    compartment1_x = 30,
    compartment2_x = 25,
    slide_buffer = 0.5,
    lid_wall_thickness = 1.5
);

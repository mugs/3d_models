include <nesting_bin_3_well.scad>
 
 nesting_bin_3_well(
    lid_size = [95, 69, 30],
    bottom_text = "Coins, Debts, Embargoes",
    text_size = 5,
    bin_outer_wall_thickness = 1.5,
    bin_inner_wall_thickness = 1,
    compartment1_x = 30,
    compartment2_x = 25,
    slide_buffer = 0.5,
    lid_wall_thickness = 1.5
);

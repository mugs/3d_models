length = 50;
width = 10;
height = 3;

intermediate_length_1 = 10;
intermediate_width_1 = 6;

intermediate_length_2 = 15;
intermediate_width_2 = 8;

intermediate_length_3 = 21;
intermediate_width_3 = 9.75;


points = [
    //Top
    [0, 0], // 0
    [intermediate_length_1, intermediate_width_1 / 2], // 1
    [intermediate_length_2, intermediate_width_2 / 2], // 2
    [intermediate_length_3, intermediate_width_3 / 2], // 2
    [length / 2, width / 2], // 3
    [length - intermediate_length_3, intermediate_width_3 / 2], // 4
    [length - intermediate_length_2, intermediate_width_2 / 2], // 4
    [length - intermediate_length_1, intermediate_width_1 / 2], // 5
    [length, 0],  // 6
    [length - intermediate_length_1, 0 - intermediate_width_1 / 2], // 7
    [length - intermediate_length_2, 0 - intermediate_width_2 / 2], // 8
    [length - intermediate_length_3, 0 - intermediate_width_3 / 2], // 8
    [length / 2, 0 - width / 2], // 9
    [intermediate_length_3, 0 - intermediate_width_3 / 2], // 10
    [intermediate_length_2, 0 - intermediate_width_2 / 2], // 10
    [intermediate_length_1, 0 - intermediate_width_1 / 2], // 11

];

faces = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
];

linear_extrude(height = 3, center = true, convexity = 10) {
    polygon(points);
}

translate([length / 2, 0, 0]) {
    linear_extrude(2.5)
    {
        text(text = "Juliet", size = 6, valign = "center", halign = "center");
    }
}
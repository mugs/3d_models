include <roundedcube.scad>

bottom_text = "Age Cards";
text_size = 5;
card_x = 69;
card_y = 46;
card_z = 40;
card_incursion = 2.5; // how far into the rounded corners the card well should go
bottom_thickness = 1.5;
roundedness = 5;
card_buffer = 2;

wall_thickness = 2;
top_thickness = 2;
outer_roundedness = roundedness + wall_thickness; 
slide_buffer = 0.5;

// These are actually the dimentions of the cavity, but I'm keeeping the 
// variable names consistent with card_tray.scad
box_length = card_x + card_buffer + (roundedness * 2) - (card_incursion * 2) + slide_buffer;
box_width = card_y + card_buffer + (roundedness * 2) - (card_incursion * 2) + slide_buffer;
box_height = card_z + bottom_thickness + roundedness * 2;

// These are the actual outer dimensions of the box (before cutting off the rounded top and bottom)
// Adding wall_thickness * 2 to each dimension represents the difference in the roundedness of the inner
// and outer boxes
outer_box_length = box_length + (wall_thickness * 2) - slide_buffer;
outer_box_width = box_width + (wall_thickness * 2) - slide_buffer;
//outer_box_height = box_height + (wall_thickness * 2) + top_thickness;
outer_box_height = card_z + bottom_thickness + (outer_roundedness * 2) + top_thickness;
echo(outer_box_height)

difference() {

    translate([0, 0, 0 - outer_roundedness]) {
        difference() {
            union() {
                roundedcube([outer_box_length, outer_box_width, outer_box_height], false, outer_roundedness);
            }
            //Remove the rounded top and bottom of the rounded cube to leave a rounded rectangle
            union() {
                cube([outer_box_length, outer_box_width, outer_roundedness]);
                
                translate([0, 0, outer_box_height - outer_roundedness]) {
                    cube([outer_box_length, outer_box_width, outer_roundedness]);
                }
            }
        }
    }
    
    union () {
        // Subtract a rounded rectangle the size of the inner box (plus the slide buffer)
        translate([wall_thickness, wall_thickness, top_thickness - roundedness]) {
            difference() {

                union() {
                    roundedcube([box_length, box_width, box_height], false, roundedness);
                }
                
                // subtract the bottom and top rounded portions to turn rounded cube
                // into rounded rectangle
                union() {
                    
                    cube([box_length, box_width, roundedness]);
                    
                    translate([0, 0, box_height -  roundedness]) {
                        cube([box_length, box_width, roundedness]);
                    }
                }
            }               
        }
        
        translate([outer_box_length / 2, outer_box_width / 2, 0.95]) {
            rotate([180, 0, 180]) {
                linear_extrude(1) {
                    text(bottom_text, text_size, halign="center");
                }
            }
        }
    }
}               

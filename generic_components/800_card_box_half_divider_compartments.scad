width = 95;
length = 177;
height = 55;
floor_thickness = 1.2;
divider_thickness = 3;
divider_support_thickness = 1.2;
divider_support_width = 10;

compartment_divider_height = 43;
compartment_divider_thickness = 1.2;

cube([length, width, floor_thickness]);

translate([0, (width - divider_thickness) / 2, 0]) {
    cube([length, divider_thickness, height]);
}

translate([0, (width - divider_support_width) / 2, 0]) {
    cube([divider_support_thickness, divider_support_width, height]);
}

translate([length - divider_support_thickness, (width - divider_support_width) / 2, 0]) {
    cube([divider_support_thickness, divider_support_width, height]);
}

translate([35, 0, 0]) {
    cube([compartment_divider_thickness, width, compartment_divider_height]);
}

translate([70, 0, 0]) {
    cube([compartment_divider_thickness, width, compartment_divider_height]);
}


translate([105, 0, 0]) {
    cube([compartment_divider_thickness, width, compartment_divider_height]);
}


translate([140, 0, 0]) {
    cube([compartment_divider_thickness, width, compartment_divider_height]);
}



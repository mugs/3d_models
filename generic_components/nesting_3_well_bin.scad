include <roundedcube.scad>

bottom_text = "Bits Bits Bits";
text_size = 5;

box_x = 192;
box_y = 110;
box_z = 45;

wall_thickness = 2;


inner_wall_thickness = 2;
bottom_thickness = 1.2;
outside_roundedness = 5;
inside_roundedness = 7;
compartment1_x = 62;
compartment2_x = 62;

total_box_z = box_z + (outside_roundedness * 2);

difference() {

    union() {
        translate([0, 0, 0 - outside_roundedness]) {
            difference() {
                union() {
                    roundedcube([box_x, box_y, total_box_z], false, outside_roundedness);
                }
                
                union() {
                    cube([box_x, box_y, outside_roundedness]);

                    translate([0, 0, box_z + outside_roundedness]) {
                        cube([box_x, box_y, outside_roundedness]);
                    }
                }
            }
        }
    }

    union ()
    {
        translate([wall_thickness, wall_thickness, bottom_thickness]) {
            roundedcube([compartment1_x, box_y - (wall_thickness * 2), 
                box_z + inside_roundedness], false, inside_roundedness);
        }
        translate([wall_thickness + compartment1_x + inner_wall_thickness, wall_thickness, bottom_thickness]) {
            roundedcube([compartment2_x, box_y - (wall_thickness * 2), 
                box_z + inside_roundedness], false, inside_roundedness);
        }

        compartment3_x = box_x - ((wall_thickness * 2) + compartment1_x + (2 * inner_wall_thickness) + compartment2_x);
        translate([wall_thickness + compartment1_x + inner_wall_thickness + compartment2_x + inner_wall_thickness, wall_thickness, bottom_thickness]) {
            roundedcube([compartment3_x, box_y - (wall_thickness * 2), 
                box_z + inside_roundedness], false, inside_roundedness);
        }

        translate([box_x / 2, box_y / 2, 0.95]) {
            rotate([180, 0, 180]) {
                linear_extrude(1) {
                    text(bottom_text, text_size, halign="center");
                }
            }
        }

    }


}

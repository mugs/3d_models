bottom_level_height = 7;
level_height = 10;
level_depth = 30;
width = level_depth * 5;
depth = level_depth * 4;

hole1_x = level_depth / 2;
hole2_x = hole1_x + level_depth;
hole3_x = hole2_x + level_depth;
hole4_x = hole3_x + level_depth;
hole5_x = hole4_x + level_depth;

row1_y = level_depth / 2;
row1_z = 2;
row2_y = row1_y + level_depth;
row2_z = row1_z + level_height;
row3_y = row2_y + level_depth;
row3_z = row2_z + level_height;
row4_y = row3_y + level_depth;
row4_z = row3_z + level_height;

cylinder_height = 10;
cylinder_radius = 13.75;


union() {
    difference() {
        cube([width, depth, bottom_level_height]);

        union() {
            translate([hole1_x,row1_y,row1_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole2_x,row1_y,row1_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole3_x,row1_y,row1_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole4_x,row1_y,row1_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole5_x,row1_y,row1_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
        
        }
    }
    
    difference() {
        translate([0, level_depth * 1, 0]) {
            cube([width, depth - (level_depth * 1), bottom_level_height + (level_height * 1)]);
        }
        
        union() {
            translate([hole1_x,row2_y,row2_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole2_x,row2_y,row2_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole3_x,row2_y,row2_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole4_x,row2_y,row2_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole5_x,row2_y,row2_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }        
        }
    }

    difference() {
        translate([0, level_depth * 2, 0]) {
            cube([width, depth - (level_depth * 2), bottom_level_height + (level_height * 2)]);
        }
        
        union() {
            translate([hole1_x,row3_y,row3_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole2_x,row3_y,row3_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole3_x,row3_y,row3_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole4_x,row3_y,row3_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole5_x,row3_y,row3_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }        
        }
    }

    difference() {
        translate([0, level_depth * 3, 0]) {
            cube([width, depth - (level_depth * 3), bottom_level_height + (level_height * 3)]);
        }
        
        union() {
            translate([hole1_x,row4_y,row4_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole2_x,row4_y,row4_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole3_x,row4_y,row4_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole4_x,row4_y,row4_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }
            translate([hole5_x,row4_y,row4_z]) {
                cylinder(cylinder_height, cylinder_radius, cylinder_radius, false);
            }        
        }
    }


}

include <prism.scad>

x_size = 64;
y_size = 64;
z_size = 114;

wall_thickness = 2;

door_height = 35;

baffle_side_length = 28;
baffle_y_offset = 20.2;
baffle_z_offset = 20;

difference() {
    union() {
        difference() {
            cube([x_size, y_size, z_size]);
            
            union() {
                translate([wall_thickness, wall_thickness, -1]) {
                    cube([x_size - (2*wall_thickness), y_size - (2*wall_thickness), z_size + 2]);
                }
                translate([wall_thickness, -1, 0]) {
                    cube([x_size - (2*wall_thickness), wall_thickness + 2, door_height]);
                }
            }
        }
        

        translate([wall_thickness, y_size, 0]) {
            rotate([0, 0, 270]) {
                prism(x_size - (2*wall_thickness), y_size - (2*wall_thickness) - 1, door_height);
            }
        }
        
        translate([0, baffle_y_offset, baffle_z_offset + door_height]) {
            rotate([0, 45, 270]){
                prism(x_size, baffle_side_length, baffle_side_length);
            }
        }
        
        translate([x_size, y_size - baffle_y_offset, (baffle_z_offset * 2) + door_height]) {
            rotate([0, 45, 90]){
                prism(x_size, baffle_side_length, baffle_side_length);
            }
        }

        translate([0, baffle_y_offset, (baffle_z_offset * 3) + door_height]) {
            rotate([0, 45, 270]){
                prism(x_size, baffle_side_length, baffle_side_length);
            }
        }
        
    }

    translate([-1, y_size - 8, -.0000001]) {
        cube([x_size + 2, 3, 25]);
    }
}


include <roundedcube.scad>

bottom_text = "Age Cards";
text_size = 5;
card_x = 69;
card_y = 46;
card_z = 40;
card_incursion = 2.5; // how far into the rounded corners the card well should go
bottom_thickness = 1.5;
roundedness = 5;
card_buffer = 2;

wall_x = 20;
wall_y = 12;

box_length = card_x + card_buffer + (roundedness * 2) - (card_incursion * 2);
box_width = card_y + card_buffer + (roundedness * 2) - (card_incursion * 2);
box_height = card_z + bottom_thickness + (roundedness * 2);
echo(box_height);


difference() {

    union() {
        roundedcube([box_length, box_width, box_height], false, roundedness);
    }
    
    union() {
        
        cube([box_length, box_width, roundedness]);
        
        translate([0, 0, box_height - roundedness]) {
            cube([box_length, box_width, roundedness + 1]);
        }
        translate([0, 0, roundedness]) {
            translate([roundedness - card_incursion, roundedness - card_incursion, bottom_thickness]) {
                cube([card_x + card_buffer, card_y + card_buffer, box_height]);
            }
            translate([wall_x, 0, bottom_thickness]) {
                cube([box_length - (wall_x *2), box_width, box_height]);
            }
            translate([0, wall_y, bottom_thickness]) {
                cube([box_length, box_width - (wall_y * 2), box_height]);
            }
        }
        
        translate([box_length / 2, box_width / 2, roundedness + 0.95]) {
            rotate([180, 0, 180]) {
                linear_extrude(1) {
                    text(bottom_text, text_size, halign="center");
                }
            }
        }
        
    }

}               

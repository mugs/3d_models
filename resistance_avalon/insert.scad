

difference() {
    cube([192, 140, 40]);
    
    union() {
        
        //vote tokens
        translate([35, 5, 1]) {
            cube([55, 52, 40]);
            
        }
        translate([90,30,1]) {
            cylinder(40, 15, 15, false);
        }
        translate([35,30,1]) {
            cylinder(40, 15, 15, false);
        }
        
                
        //cards
        translate([2, 64, 20]) {
            cube([98, 70, 40]);
            
        }
        translate([100,99,1]) {
            cylinder(40, 15, 15, false);
        }
        translate([12, 74, 1]) {
            cube([78, 50, 40]);
            
        }

        //pit
        translate([120, 5, 1]) {
            cube([67, 130, 40]);
            
        }

    }
    
}
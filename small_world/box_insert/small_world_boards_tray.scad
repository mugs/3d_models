wow_board_x = 243;
wow_board_y = 243;
wow_board_z = 18;

small_board_x = 263;
small_board_y = 263;
small_board_z = 17;


big_board_x = 283;
big_board_y = 263;
big_board_z = 13;


floor_thickness = 1;
total_x = 304;
total_y = 304;
total_z = big_board_z + small_board_z + wow_board_z + floor_thickness;

kill_wall_x = 205;
kill_wall_y = 200;

kill_floor_xy = 200;

difference() {
    
    cube([total_x, total_y, total_z]);
    
    
    union() {
        x_offset = (total_x - wow_board_x) / 2;
        y_offset = (total_y - wow_board_y) / 2;
        
        translate([x_offset, y_offset, floor_thickness]) {
            cube([wow_board_x, wow_board_y, total_z]);
            
        }

        x_offset2 = (total_x - small_board_x) / 2;
        y_offset2 = (total_y - small_board_y) / 2;
        
        translate([x_offset2, y_offset2, floor_thickness + wow_board_z]) {
            cube([small_board_x, small_board_y, total_z]);
            
        }
        
        x_offset3 = (total_x - big_board_x) / 2;
        y_offset3 = (total_y - big_board_y) / 2;
        
        translate([x_offset3, y_offset3, floor_thickness + wow_board_z + small_board_z]) {
            cube([big_board_x, big_board_y, total_z]);
            
        }
        
        x_offset4 = (total_x - kill_wall_x) / 2;
        translate([x_offset4, 0, floor_thickness]) {
            cube([kill_wall_x, total_y, total_z]);
            
        }
        y_offset4 = (total_y - kill_wall_y) / 2;
        translate([0, y_offset4, floor_thickness]) {
            cube([total_x, kill_wall_y, total_z]);
            
        }
        
        xy_offset = (total_x - kill_floor_xy) / 2;
        translate([xy_offset, xy_offset, 0]) {
            cube([kill_floor_xy, kill_floor_xy, total_z]);
            
        }
        
    }
    
    
    
    
    
    
}
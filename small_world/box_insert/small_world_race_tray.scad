
total_width = 100;
race_box_width = 85;
total_length = 152;
end_wall_width = 1;
bottom_thickness = 1;
total_height = 20;
floor_inset = 12;


race_box_left_edge_x = (total_width - race_box_width) / 2;

difference() {
    
    union() {
        cube([total_width, total_length, total_height]);
        
        
    }
    
    union() {
        translate([race_box_left_edge_x, end_wall_width, bottom_thickness]) {
            cube([race_box_width, total_length - (end_wall_width * 2), total_height]);
        }
        translate([race_box_left_edge_x + floor_inset, end_wall_width + floor_inset, 0]) {
            cube([race_box_width - (floor_inset * 2), total_length - (end_wall_width * 2) - (floor_inset * 2), total_height]);
        }
        
    }
    
}










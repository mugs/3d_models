
total_width = 100;
race_box_width = 85;
total_length = 152;
end_wall_width = 1;
bottom_thickness = 1;
total_height = 20;
floor_inset = 12;


race_box_left_edge_x = (total_width - race_box_width) / 2;

difference() {
    
    union() {
        cube([total_width, total_length, total_height]);
        
        
    }
    
    union() {
        translate([20, 10, bottom_thickness]) {
            cube([60, 65, total_height]);
        }
        
        translate([20, 85, bottom_thickness]) {
            cube([60, 58, total_height]);
        }
        
    }
    
}










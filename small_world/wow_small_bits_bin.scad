include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 76, 20],
        bottom_text = "WOW Bits",
        text_size = 10,
        skip_compartments = true
    );
    
    union() {
        

        translate([1.5, 1.5, 1.5]) {
            roundedcube([48, 69, 100], false, compartment_roundedness);
        }
        
        translate([50.5, 1.5, 1.5]) {
            roundedcube([48, 69, 100], false, compartment_roundedness);
        }

        
        translate([99.5, 1.5, 1.5]) {
            roundedcube([47, 69, 100], false, compartment_roundedness);
        }
        
    }
    
}
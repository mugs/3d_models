//esta tiene solo 3 capas de base

$fn=100;
grosor=2;   //mm de grosor del muro
fichas=8;  //total de tokens (supone 2mm altura)
            //!!!!!!!!!
            // que redondee al siguiente par
            // 16->16, 17->18, 18->18
            
// 8 tokens: Apariciones (7+1)

//10 tokens: Enanos (8+1) 9-^10
//           Clérigos (9+1)

//12 tokens: 12 races total
//14 tokens: 2
//16 tokens: 6 (incloent leperchauns i skags)
//18 tokens: 3 (inclou brujas de hielo)
//20 tokens: 2
//22 tokens: 2 (els dos son 20+1, el lider pot anar en diagonal i serien 20 tokens nomes)

            
raza=2;		//mm altura raza
            
module placa(abierto=false, x=86, y=45) {
	offset(delta=0.1) //apariciones: -0.9
                       //anterior: -1
	union(){
		difference(){
			union(){
				translate([y/5, y/5])
					circle(y/5);
				translate([y/2, y/2])
					circle(y/2);
				polygon([
					[0,y/5],
					[0,y/2],
					[y/2,y],
					[x,y],
					[x-6,0],
					[y/5,0]]);
			}
			translate([x*0.875,0])
				translate([y/2,y/2])
					circle(y/2);
		}
		if(abierto)
			translate([2*x/3,0,0])
                union(){
                    translate([y/5, y/5])
                        circle(y/5);
                    translate([y/2, y/2])
                        circle(y/2);
                    polygon([
                        [0,y/5],
                        [0,y/2],
                        [y/2,y],
                        [x,y],
                        [x-6,0],
                        [y/5,0]]);
                }
	}
}


translate([0,0,-0.25])
difference() {
    union() {

		//base: 4 capas * 0.25mm
	
//		translate([0, 0, 0])
//			linear_extrude(0.25)
//				offset(delta=-1)
//					placa();

		translate([0, 0, 0.25])
			linear_extrude(0.25)
				offset(delta=-0.75)
					placa();
		
		translate([0, 0, 0.5])
			linear_extrude(0.25)
				offset(delta=-0.5)
					placa();
		
		translate([0, 0, 0.75])
			linear_extrude(0.25)
				offset(delta=-0.25)
					placa();
		
		//curva: 4 capas * 0.25mm
		
		translate([0, 0, 1])
			linear_extrude(0.25)
				resta(0, -2);

		translate([0, 0, 1.25])
			linear_extrude(0.25)
				resta(0.25, -1.75);

		translate([0, 0, 1.5])
			linear_extrude(0.25)
				resta(0.5, -1.5);

		translate([0, 0, 1.75])
			linear_extrude(0.25)
				resta(0.75, -1.25);

		//cuerpo: fichas-1 mm
		
		translate([0, 0, 2])
			linear_extrude(fichas-1)
				resta(1, -1);
		
		//pasador: raza mm
		
		translate([0, 0, fichas+1])
			linear_extrude(raza)
				resta(1, 0, true);
		
		//tope: 3 capas * 0.25mm + 1 capa * 1mm
		
		translate([0, 0, fichas+raza+1])
			linear_extrude(0.25)
				resta(1, -0.25, true);
		
		translate([0, 0, fichas+raza+1.25])
			linear_extrude(0.25)
				resta(1, -0.5, 1);
		
		translate([0, 0, fichas+raza+1.5])
			linear_extrude(0.25)
				resta(1, -0.75, 1);
		
		translate([0, 0, fichas+raza+1.75])
			linear_extrude(1)
				resta(1, -1, 1);
		
	}
	
	//translate([60,-10,0])
		//cube(80);
//    translate([10,22,0.75])
//          linear_extrude(1)
//              text("APARICIONES", font="Vafthrudnir:style=Regular", size=6);
//        translate([25,10,0.75])
//            linear_extrude(1)
//                text("7 + 1", font="ManuskriptGotisch:style=Regular", size=10);
    
}

module resta(exterior, interior, abierto=false){
	difference(){
		offset(delta=exterior)
			placa();
		offset(delta=interior)
			placa(abierto);
	}
}
include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 152, 20],
        bottom_text = "Small World Bits",
        text_size = 10,
        skip_compartments = true
    );
    
    union() {
        
        // big daddy bin
        translate([1.5, 1.5, 1.5]) {
            roundedcube([97, 97, 100], false, compartment_roundedness);
        }
        
        // medium mama bin
        translate([99.5, 1.5, 1.5]) {
            roundedcube([48, 97, 100], false, compartment_roundedness);
        }

        translate([1.5, 1.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }
        translate([1.5, 50.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }
        translate([1.5, 99.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }
        
        
        
        translate([50.5, 1.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }
        translate([50.5, 50.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }
        translate([50.5, 99.5, 1.5]) {
            roundedcube([48, 48, 100], false, compartment_roundedness);
        }

        
        translate([99.5, 1.5, 1.5]) {
            roundedcube([48, 47, 100], false, compartment_roundedness);
        }
        translate([99.5, 50.5, 1.5]) {
            roundedcube([48, 47, 100], false, compartment_roundedness);
        }
        translate([99.5, 99.5, 1.5]) {
            roundedcube([48, 47, 100], false, compartment_roundedness);
        }
        
        /*translate([37.5, 1.5, 1.5]) {
            roundedcube([35, 45, 100], false, compartment_roundedness);
        }
        translate([37.5, 48.5, 1.5]) {
            roundedcube([35, 46, 100], false, compartment_roundedness);
        }

        translate([73.5, 1.5, 1.5]) {
            roundedcube([35, 45, 100], false, compartment_roundedness);
        }
        translate([73.5, 48.5, 1.5]) {
            roundedcube([35, 46, 100], false, compartment_roundedness);
        }

        translate([109.5, 1.5, 1.5]) {
            roundedcube([35, 45, 100], false, compartment_roundedness);
        }
        translate([109.5, 48.5, 1.5]) {
            roundedcube([35, 46, 100], false, compartment_roundedness);
        }
        */
        
    }
    
}
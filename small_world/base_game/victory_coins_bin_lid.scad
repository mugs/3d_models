include <nesting_bin_lid_3_well.scad>

compartment_roundedness = 3;

nesting_bin_lid_3_well(
    lid_size = [152, 76, 25],
    bottom_text = "Victory Coins",
    text_size = 10,
    lid_wall_thickness = 1.5
);
    

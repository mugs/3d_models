include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 76, 25],
        bottom_text = "Victory Coins",
        text_size = 10,
        skip_compartments = true
    );
    
    union() {
        

        translate([1.5, 1.5, 1.5]) {
            roundedcube([72, 69, 100], false, compartment_roundedness);
        }
                
        translate([74.5, 1.5, 1.5]) {
            roundedcube([72, 69, 100], false, compartment_roundedness);
        }
        
    }
    
}
include <card_tray_lid.scad>

finger_hole_radius = 18;

difference() {
    card_tray_lid(
        card_size = [43, 46, 45],
        bottom_text = "Special Powers",
        wall_x = 55,
        wall_y = 30,
        text_size = 4
    );

    translate([27, 26, 53]) {
        rotate([90, 0, 0]) {
            cylinder(h = 110, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
        }
    }

}
include <nesting_bin_lid_3_well.scad>

compartment_roundedness = 3;

nesting_bin_lid_3_well(
        lid_size = [152, 152, 20],
        bottom_text = "Realms Bits",
        text_size = 10,
        slide_buffer = 1
);
    

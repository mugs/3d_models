include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 76, 50],
        bottom_text = "Realms Bits",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 0.75
    );
    
    union() {
        translate([1.5, 1.5, 1.5]) {
            roundedcube([145, 69, 100], false, compartment_roundedness);
        }
    }
}
include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 152, 30],
        bottom_text = "Realms Bits",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 1
    );
    
    union() {
        translate([1.5, 1.5, 1.5]) {
            roundedcube([144.5, 144.5, 100], false, compartment_roundedness);
        }
        
    }
    
}
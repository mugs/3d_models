
box_length = 127;
box_width = 87;
box_height = 72;

wall_thickness = 2;
floor_thickness = 1.6;

corner_width = 27;
bottom_barrier_height = 20;

bottom_hole_size_x = 65;
bottom_hole_size_y = 45;

difference() {
    cube([box_length, box_width, box_height]);
    
    //minus
    union() {
            
        translate([wall_thickness, wall_thickness, floor_thickness]) {
            cube([box_length - (2*wall_thickness),
                box_width - (2*wall_thickness),
                box_height]);
        };

        translate([corner_width, -1, bottom_barrier_height]) {
            cube([box_length - (2*corner_width),
                box_width + 2,
                box_height]);
        }
        translate([-1, corner_width, bottom_barrier_height]) {
            cube([box_length + 2,
                box_width  - (2*corner_width),
                box_height]);
        }
        
        translate([(box_length / 2) - (bottom_hole_size_x / 2),
            (box_width / 2) - (bottom_hole_size_y / 2),
            -1]) {
                
            cube([bottom_hole_size_x, bottom_hole_size_y, 2 + floor_thickness]);

        };
        
        
    }
}

translate([16, 0, 25]) {
    rotate([0, 45, 0]) {
        cube([25,
            wall_thickness,
            15]);
    };
}

translate([16, box_width - wall_thickness, 25]) {
    rotate([0, 45, 0]) {
        cube([25,
            wall_thickness,
            15]);
    };
}

translate([95, 0, 7]) {
    rotate([0, -45, 0]) {
        cube([25,
            wall_thickness,
            15]);
    };
}

translate([95, box_width - wall_thickness, 7]) {
    rotate([0, -45, 0]) {
        cube([25,
            wall_thickness,
            15]);
    };
}


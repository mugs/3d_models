include <card_tray.scad>

finger_hole_radius = 18;

difference() {
    card_tray(
        card_size = [85, 58, 58],
        bottom_text = "Home Planets, Phases, Generic Tiles",
        wall_x = 70,
        wall_y = 50,
        text_size = 3.9
    );

    translate([30.5, 32, 62]) {
        rotate([0, 90, 0]) {
            cylinder(h = 150, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
        }
    }
}
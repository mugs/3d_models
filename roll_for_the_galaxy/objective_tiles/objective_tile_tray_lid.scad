include <card_tray_lid.scad>

finger_hole_radius = 18;

difference() {
    card_tray_lid(
        card_size = [77, 48, 58],
        bottom_text = "Objective Tiles",
        wall_x = 70,
        wall_y = 50,
        text_size = 5
    );

    translate([42.5, 26, 64]) {
        rotate([90, 0, 0]) {
            cylinder(h = 110, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
        }
    }
}



include <prism.scad>

//These dimensions from Dice_tray.scad are necessary to calculate the dimensions of the lid
tray_bottom_thickness = 2;
tray_outer_wall_height = 2;
tray_inner_lid_wall_thickness = 2;
bevel_height = 1.4;
bevel_depth = 1.4;


lid_top_thickness = 2;
lid_wall_thickness = 2;
// The overhang is the part of the lid that doesn't have sides
lid_overhang_length = 14;

die_size = 12.4;

//These gaps are additional space added to all of the material thicknesses to allow the tray and lid to slide together and apart more easily
top_gap = 0.2;
side_gap = 0.3;

//Number of dice in each directions.  The lid slides in the X direction (this is where the bevels are)
dice_x = 7;
dice_y = 3;

box_length = (2*tray_outer_wall_height) + lid_wall_thickness + ((dice_x - 1) * tray_inner_lid_wall_thickness) + (dice_x * die_size);
box_width = (2*tray_outer_wall_height) + (2*lid_wall_thickness) + ((dice_y - 1) * tray_inner_lid_wall_thickness) + (dice_y * die_size) + side_gap;
box_height = tray_bottom_thickness + lid_top_thickness + die_size + top_gap;



difference() {

    cube(
        [ box_length,
          box_width,
          box_height
        ]
    );
    
    union() {
        translate([lid_wall_thickness, lid_wall_thickness, tray_bottom_thickness]) {
            cube(
                [ box_length - lid_wall_thickness,
                  box_width - (lid_wall_thickness * 2),
                  box_height - lid_top_thickness
                ]
            );
        }
        translate([box_length - lid_overhang_length, 0, tray_bottom_thickness]) {
            cube(
                [ lid_overhang_length,
                  box_width,
                  box_height - lid_top_thickness
                ]
            );
        }
    }
}


//Bevels
translate([box_length - lid_overhang_length, lid_wall_thickness, box_height]) {
    rotate([0, 90, 90]) {
        prism(box_length - lid_overhang_length, bevel_depth, bevel_height);
    }
}

translate([box_length - lid_overhang_length, box_width - lid_wall_thickness, box_height]) {
    rotate([0, 180, 90]) {
        prism(box_length - lid_overhang_length, bevel_depth, bevel_height);
    }
}

include <card_tray_lid.scad>

finger_hole_radius = 25;

difference() {
    card_tray_lid(
        card_size = [116, 68, 58],
        bottom_text = "Faction Tiles",
        wall_x = 70,
        wall_y = 50,
        text_size = 5
    );

    translate([62, 26, 62]) {
        rotate([90, 0, 0]) {
            cylinder(h = 110, r1 = finger_hole_radius, r2 = finger_hole_radius, center = true);
        }
    }
}



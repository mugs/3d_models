include <prism.scad>

// Tall outer wall dimensions
tray_outer_wall_height = 9;
tray_outer_wall_thickness = 2;

// Short inner wall dimensions
tray_inner_wall_height = 2;
tray_inner_wall_thickness = 2;

tray_bottom_thickness = 2;
bevel_height = 1.4;
bevel_depth = 1.4;

// Size of dice cavities
die_size = 12.4;

// Number of dice in each direction.  Bevels will be along the X side
dice_x = 7;
dice_y = 3;

//X,Y coordinates of the dice cavities that should be filled in, e.g.:
//skip_cavities = [[3,2], [4,2]];
skip_cavities = [];

tray_total_x = (2*tray_outer_wall_thickness) + ((dice_x - 1) * tray_inner_wall_thickness) + (dice_x * die_size);
tray_total_y = (2*tray_outer_wall_thickness) + ((dice_y - 1) * tray_inner_wall_thickness) + (dice_y * die_size);



difference() {

    cube(
        [ tray_total_x,
          tray_total_y,
          tray_outer_wall_height
        ]
    );
    
    union() {
        // Big cavity
        translate([tray_outer_wall_thickness, tray_outer_wall_thickness, tray_bottom_thickness + tray_inner_wall_height]) {
            cube(
                [  ((dice_x - 1) * tray_inner_wall_thickness) + (dice_x * die_size),
                   ((dice_y - 1) * tray_inner_wall_thickness) + (dice_y * die_size),
                   tray_outer_wall_height
                ]
            );
        }
        
        //Bevels; I had to offset these by 0.0001 milimeters to avoid "WARNING: Object may not be a valid 2-manifold and may need repair!"
        // for some larger box sizes, not sure what's up with that.
        translate([tray_total_x + 1, -0.0001, 0]) {
            rotate([0, 0, 90]) {
                prism(tray_total_x + 2, bevel_depth, bevel_height);
            }
        }
        translate([-1, tray_total_y + 0.0001, 0]) {
            rotate([0, 0, 270]) {
                prism(tray_total_x + 2, bevel_depth, bevel_height);
            }
        }

        // Dice cavities
        for(i = [0:(dice_x - 1)]) {
            for(j = [0:(dice_y - 1)]) {
                translate(
                    [ tray_outer_wall_thickness + (i * die_size) + (i * tray_inner_wall_thickness),
                      tray_outer_wall_thickness + (j * die_size) + (j * tray_inner_wall_thickness),
                      tray_bottom_thickness
                    ]
                ) {
                    cube([die_size, die_size, die_size]);
                }
                
            }
        }

    }
}

for(k = skip_cavities) {
    translate(
        [ tray_outer_wall_thickness + (k[0] * die_size) + (k[0] * tray_inner_wall_thickness),
          tray_outer_wall_thickness + (k[1] * die_size) + (k[1] * tray_inner_wall_thickness),
          tray_bottom_thickness
        ]
    ) {
        cube([die_size, die_size, tray_inner_wall_height]);
    }
}

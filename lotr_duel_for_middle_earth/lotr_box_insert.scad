include <roundedcube.scad>

$fn = $preview ? 12 : 72;

total_x = 196;
total_y = 261;
total_z = 45;


card_box_distance_from_top = 3;
card_box_x = 189;
card_box_y = 96;
card_box_z = 26;

bits_box_distance_from_top = card_box_distance_from_top + card_box_y + 5;
bits_box_distance_from_left = 3;
bits_box_x = 91;
bits_box_y = 154;
bits_box_z = 26;

landmark_box_distance_from_top = card_box_distance_from_top + card_box_y + 35;
landmark_box_distance_from_left = bits_box_distance_from_left + bits_box_x + 7;

landmark_box_x = 90;
landmark_box_y = 90;
landmark_box_z = 19;


quest_track_distance_from_top = 35;

quest_track_x = 54;
quest_track_y = 197;
quest_track_z = 10;
quest_track_distance_from_left = bits_box_distance_from_left + ((bits_box_x - quest_track_x) / 2);

finger_hole_roundedness = 4;
finger_hole_width = 50;


difference() {
    union() {
        cube([total_x, total_y, total_z]);
    }
    
    
    union() {
        translate([(total_x - card_box_x) / 2, total_y - card_box_y - card_box_distance_from_top, total_z - card_box_z]) {
            cube([card_box_x, card_box_y, card_box_z]);
        }
        
        translate([bits_box_distance_from_left, total_y - bits_box_y - bits_box_distance_from_top, total_z - bits_box_z]) {
            cube([bits_box_x, bits_box_y, bits_box_z]);
        }
        
        translate([landmark_box_distance_from_left, total_y - landmark_box_y - landmark_box_distance_from_top, total_z - landmark_box_z]) {
            cube([landmark_box_x, landmark_box_y, landmark_box_z]);
        }
        
        
        translate([quest_track_distance_from_left, 
            total_y - quest_track_y - quest_track_distance_from_top, 
            total_z - bits_box_z - quest_track_z]) {
                
            cube([quest_track_x, quest_track_y, 50]);
        }
        
        
        // finger holes
        
        translate([bits_box_distance_from_left, 
            total_y - landmark_box_distance_from_top - landmark_box_y + (finger_hole_width / 2),
            3]) {
            
            roundedcube([total_x - 8, finger_hole_width, 60], false, finger_hole_roundedness);
            
        }
        
        translate([landmark_box_distance_from_left + ((landmark_box_y - finger_hole_width) / 2), 
            5, 3]) {
            
            roundedcube([finger_hole_width, total_y - 10, 60], false, finger_hole_roundedness);
            
        }
        
        
        
        
        
        
    }
    
}
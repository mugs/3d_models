include <card_tray_lid.scad>

$fn = $preview ? 12 : 0;


card_tray_lid(
	card_size = [78, 78, 15],
	bottom_text = "Landmarks",
	wall_x = 14,
	wall_y = 14,
	text_size = 5,
    slide_buffer = 0.6
);


include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [152, 90, 26],
        bottom_text = "Bits",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 0.6
    );
    
    union() {
        

        translate([1.5, 1.5, 1.5]) {
            roundedcube([48, 83, 100], false, compartment_roundedness);
        }
        
        translate([50.5, 1.5, 1.5]) {
            roundedcube([48, 83, 100], false, compartment_roundedness);
        }

        
        translate([99.5, 1.5, 1.5]) {
            roundedcube([47, 83, 100], false, compartment_roundedness);
        }
        
    }
    
}
include <nesting_bin_lid_3_well.scad>

compartment_roundedness = 3;

nesting_bin_lid_3_well(
    lid_size = [152, 90, 26],
    bottom_text = "Bits",
    text_size = 10,
    lid_wall_thickness = 1.5,
    slide_buffer = 0.6
);
    

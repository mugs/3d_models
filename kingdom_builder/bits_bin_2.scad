include <nesting_bin_3_well.scad>
 
 $fn = $preview ? 12 : 0;

 
 difference() {
        
     nesting_bin_3_well(
        lid_size = [90, 75, 19],
        bottom_text = "",
        text_size = 5,
        bin_outer_wall_thickness = 1.5,
        bin_inner_wall_thickness = 1,
        skip_compartments = true,
        slide_buffer = 0.7,
        lid_wall_thickness = 1.5
    );

    translate([1.4, 1.4, 1.0]) {
        // x - 6.7, y - 6.7
        roundedcube([35, 68.5, 300], false, 3);
    }
    translate([37.4, 1.4, 1.0]) {
        // x - 6.7, y - 6.7
        roundedcube([48.5, 68.5, 300], false, 3);
    }
};
total_x = 150;
total_y = 151;
bottom_thickness = 2;

hex_width = 25;
half_hex_width = hex_width / 2;
hex_side = 15;
hex_height_angled_side = 6.934;
hex_depth_angled_side = 6.934;

top_size = 14.5;
//bottom_size = 65;
left_size = 19.25;

offset = 0.5;

vertical_height = 78;
//vertical_height = 10;

bonus_tile_height = 70;
bonus_tile_width = 77;

finger_space_width = 25;
finger_space_height = 20;

//total_big_part_width = left_size + (3 * hex_width);

x1 = 0;
y1 = 0;

x2 = 0;
y2 = top_size + (2 * hex_height_angled_side) + (2 * hex_side) + offset;

//x3 = x2 + 1;
//y3 = y2;

//x4 = x3 + 1;
//y4 = y3;

//x5 = x4 + 1;
//y5 = y4;

//x6 = x5 + 1;
//y6 = y5;

x7 = left_size;
y7 = top_size + (2 * hex_height_angled_side) + (2 * hex_side) + offset;

x8 = x7;
y8 = top_size + (2 * hex_height_angled_side) + (1 * hex_side) - offset;

//x9 = x6;
x9 = x8;
//y9 = top_size + (1 * hex_height_angled_side) + (1 * hex_side) - offset;
y9 = top_size - offset;

//x10 = x9;
x10 = x7;
//y10 = top_size + (1*hex_height_angled_side) - offset;
y10 = top_size - offset;

x11 = left_size + (hex_width);
y11 = top_size - offset;

x12 = left_size + (1.5 * hex_width);
//y12 = y10;
y12 = top_size + (1*hex_height_angled_side) - offset;

x13 = left_size + (2 * hex_width);
y13 = y11;

x14 = left_size + (2.5 * hex_width);
y14 = y12;

x15 = left_size + (3 * hex_width);
y15 = y13;

x16 = x15;
y16 = 0;

z1 = 0;
z2 = vertical_height;


points = [

	//top
	[x1, y1, z1], // 0
	[x2, y2, z1], // 1
	//[x3, y3, z1], // 2
	//[x4, y4, z1], // 3
	//[x5, y5, z1], // 4
	//[x6, y6, z1], // 5
	[x7, y7, z1], // 6
	[x8, y8, z1], // 7
	[x9, y9, z1], // 8
	[x10, y10, z1], // 9
	[x11, y11, z1], // 10
	[x12, y12, z1], // 11
	[x13, y13, z1], // 12
	[x14, y14, z1], // 13
	[x15, y15, z1], // 14
	[x16, y16, z1], // 15

	// bottom
	[x1, y1, z2], // 16
	[x2, y2, z2], // 17
	//[x3, y3, z2], // 18
	//[x4, y4, z2], // 19
	//[x5, y5, z2], // 20
	//[x6, y6, z2], // 21
	[x7, y7, z2], // 22
	[x8, y8, z2], // 23
	[x9, y9, z2], // 24
	[x10, y10, z2], // 25
	[x11, y11, z2], // 26
	[x12, y12, z2], // 27
	[x13, y13, z2], // 28
	[x14, y14, z2], // 29
	[x15, y15, z2], // 30
	[x16, y16, z2], // 31
];

faces = [
	[11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0], // bottom
	[12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23], //top	
	
	//starting on left side and moving clockwise
	[0, 1, 13, 12],
	[1, 2, 14, 13],
	[2, 3, 15, 14], 
	[3, 4, 16, 15],
	[4, 5, 17, 16], 
	[5, 6, 18, 17],
	[6, 7, 19, 18], 
	[7, 8, 20, 19], 
	[8, 9, 21, 20], 
	[9, 10, 22, 21],
	[10, 11, 23, 22],
	[11, 0, 12, 23],
];


union() {    	
    cube([total_x, total_y, bottom_thickness]);	
    polyhedron(points, faces);

}


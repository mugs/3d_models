include <nesting_bin_lid_3_well.scad>
 
 $fn = $preview ? 12 : 0;

 
 nesting_bin_lid_3_well(
    lid_size = [111, 75, 19],
    bottom_text = "",
    text_size = 5,
    compartment1_x = 30,
    compartment2_x = 25,
    slide_buffer = 0.7,
    lid_wall_thickness = 1.5
);

total_x = 150;
total_y = 151;
bottom_thickness = 2;

hex_width = 25;
half_hex_width = hex_width / 2;
hex_height = 29;
hex_side = 15;
hex_height_angled_side = 6.934;
hex_depth_angled_side = 6.934;

top_size = 14.5;
bottom_size = 65;
right_size = 19.25;

offset = 0.5;

vertical_height = 78;

bonus_tile_height = 70;
bonus_tile_width = 77;

finger_space_width = 25;
finger_space_height = 20;

total_big_part_width = (2 * hex_width) + half_hex_width + right_size;

x1 = 0;
y1 = 0;

x2 = 0;
y2 = top_size - offset;

x3 = half_hex_width;
y3 = top_size + hex_depth_angled_side - offset;

x4 = hex_width;
y4 = y2;

x5 = hex_width + half_hex_width;
//y5 = y3;
y5 = y4;

x6 = 2 * hex_width;
y6 = y4;

x7 = (2 * hex_width) + half_hex_width + offset;
y7 = top_size + hex_depth_angled_side - offset;

x8 = x7;
y8 = top_size + hex_depth_angled_side + hex_side + offset;

//x9 = 2 * hex_width + offset;
x9 = x8;
y9 = top_size + (2*hex_height_angled_side) + hex_side + offset;

x10 = x9;
y10 = top_size + (2*hex_height_angled_side) + (2*hex_side) - offset;

x11 = x8;
y11 = top_size + (3*hex_height_angled_side) + (2*hex_side) - offset;

x12 = x8;
y12 = top_size + (3*hex_height_angled_side) + (3*hex_side) + offset;

//x13 = x10;
x13 = 2 * hex_width + offset;
y13 = top_size + (4*hex_height_angled_side) + (3*hex_side) + offset;

x14 = x13;
y14 = top_size + (4*hex_height_angled_side) + (4*hex_side) - (3*offset); // little extra padding because it's the top

x15 = (2 * hex_width) + half_hex_width + right_size;
y15 = y8;

x16 = x15;
y16 = 0;

z1 = 0;
z2 = vertical_height;

points = [

	//top
	[x1, y1, z1], // 0
	[x2, y2, z1], // 1
	[x3, y3, z1], // 2
	[x4, y4, z1], // 3
	[x5, y5, z1], // 4
	[x6, y6, z1], // 5
	[x7, y7, z1], // 6
	[x8, y8, z1], // 7
	//[x9, y9, z1], // 8
	//[x10, y10, z1], // 9
	//[x11, y11, z1], // 10
	//[x12, y12, z1], // 11
	//[x13, y13, z1], // 12
	//[x14, y14, z1], // 13
	[x15, y15, z1], // 14
	[x16, y16, z1], // 15

	// bottom
	[x1, y1, z2], // 16
	[x2, y2, z2], // 17
	[x3, y3, z2], // 18
	[x4, y4, z2], // 19
	[x5, y5, z2], // 20
	[x6, y6, z2], // 21
	[x7, y7, z2], // 22
	[x8, y8, z2], // 23
	//[x9, y9, z2], // 24
	//[x10, y10, z2], // 25
	//[x11, y11, z2], // 26
	//[x12, y12, z2], // 27
	//[x13, y13, z2], // 28
	//[x14, y14, z2], // 29
	[x15, y15, z2], // 30
	[x16, y16, z2], // 31
];

faces = [
	[9, 8, 7, 6, 5, 4, 3, 2, 1, 0], // bottom
	[10, 11, 12, 13, 14, 15, 16, 17, 18, 19], //top	
	
	//starting on left side and moving clockwise
	[0, 1, 11, 10],
	[1, 2, 12, 11],
	[2, 3, 13, 12], 
	[3, 4, 14, 13],
	[4, 5, 15, 14], 
	[5, 6, 16, 15],
	[6, 7, 17, 16], 
	[7, 8, 18, 17], 
	[8, 9, 19, 18], 
	[9, 0, 10, 19],
];

difference() {

    union() {
    	polyhedron(points, faces);
	
		translate([total_big_part_width - total_x, 0, 0]) {
			cube([total_x, total_y, bottom_thickness]);
		}
	
	}
}
include <nesting_bin_lid_3_well.scad>

$fn = $preview ? 12 : 0;


compartment_roundedness = 3;

nesting_bin_lid_3_well(
        lid_size = [142, 114.5, 30],
        bottom_text = "",
        text_size = 10,
        slide_buffer = 1
);
    

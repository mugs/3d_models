include <card_box_lid.scad>

$fn = $preview ? 12 : 0;


card_box_lid( 
    box_size = [163, 42, 135], 
    text_line_1 = "",
    finger_slot_radius = 0,
    lip_height = 10
);

/*
compartment_roundedness = 3;

nesting_bin_lid_3_well(
        lid_size = [142, 114.5, 30],
        bottom_text = "",
        text_size = 10,
        slide_buffer = 1
);
    
*/
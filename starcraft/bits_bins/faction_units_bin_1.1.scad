include <card_box.scad>


compartment_roundedness = 3;

$fn = $preview ? 12 : 0;

card_box( 
    box_size = [163, 42, 135], 
    text_line_1 = "",
    finger_slot_radius = 0,
    lip_height = 10
);




/*
difference() {
    nesting_bin_3_well(
        lid_size = [175, 143, 45],
        bottom_text = "",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 1
    );
    
    union() {
        
        // big bin
        translate([1.5, 1.5, 1.5]) {
            roundedcube([167.5, 135.5, 100], false, compartment_roundedness);
        }
        
        
    }
    170x142 final size goal
}*/
include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

$fn = $preview ? 12 : 0;


difference() {
    nesting_bin_3_well(
        lid_size = [114.5, 142, 30],
        bottom_text = "",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 1
    );
    
    union() {
        
        // medium bin
        translate([1.5, 1.5, 1.5]) {
            roundedcube([48, 134.5, 100], false, compartment_roundedness);
        }
        

        
        
        translate([50.5, 1.5, 1.5]) {
            roundedcube([58, 44, 100], false, compartment_roundedness);
        }
        translate([50.5, 46.5, 1.5]) {
            roundedcube([58, 44, 100], false, compartment_roundedness);
        }
        translate([50.5, 91.5, 1.5]) {
            roundedcube([58, 44.5, 100], false, compartment_roundedness);
        }

        
    }
    
}
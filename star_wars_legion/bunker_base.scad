big_width = 204;
small_width = 169;
depth = 94;
height = 2;

small_x1 = (big_width - small_width) / 2;
small_x2 = small_x1 + small_width;

points = [
    //bottom
    [0, 0, 0], // 0
    [big_width, 0, 0], // 1
    [small_x2, depth, 0], // 2
    [small_x1, depth, 0], // 3
    
    //top
    [0, 0, height], // 4
    [big_width, 0, height], // 5
    [small_x2, depth, height], // 6
    [small_x1, depth, height], // 7
];

faces = [
    [0, 1, 2, 3], // bottom
    [7, 6, 5, 4], // top
    [5, 6, 2, 1], //right side
    [6, 7, 3, 2], // far side
    [7, 4, 0, 3], // left side
    [4, 5, 1, 0], //near side

    
];

polyhedron(points, faces);

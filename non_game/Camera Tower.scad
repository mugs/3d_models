

difference() {
    union() {
        
        translate([0, -30, 0]) {
            cube([100, 120, 8]);
        }
        translate([38, 0, 0]) {
            cube([24, 24, 375]);
        }
        
    }
    
    translate([42, -20, 340]) {
        rotate([-15, 0, 0]) {
            cube([16, 66, 25]);
            
        }
    }
}
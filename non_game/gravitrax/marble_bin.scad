include <nesting_bin_3_well.scad>

compartment_roundedness = 3;

difference() {
    nesting_bin_3_well(
        lid_size = [100, 100, 25],
        bottom_text = "Marbles",
        text_size = 10,
        skip_compartments = true,
        slide_buffer = 0.5
    );
    
    union() {
        translate([1.5, 1.5, 1.5]) {
            roundedcube([93, 93, 100], false, compartment_roundedness);
        }
    }
}
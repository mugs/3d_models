include <prism.scad>

box_length = 129;
box_width = 89;
box_height = 8;

x_wall_thickness = 2;
y_wall_thickness = 2;
floor_thickness = 1.2;

total_x = (2 * x_wall_thickness) + box_length;
total_y = (2 * y_wall_thickness) + box_width;
total_z = floor_thickness + box_height;

hole_size_x = 75;
hole_size_y = 45;

text_y = hole_size_y + ((3/4) * (total_y - hole_size_y));

difference() {
    cube([total_x, total_y, total_z]);
    
    union() {
        translate([x_wall_thickness, y_wall_thickness, floor_thickness]) {
            cube([box_length, box_width, box_height]);
        }
        

        translate([(total_x / 2), text_y, 0]) {
            linear_extrude(height = 0.4) {
                rotate([0, 180, 0]) {
                    text("BOARD GOES HERE", font =  "Liberation Sans:style=Bold", size = 9, halign = "center", valign = "center");
                }
            }
        }
        
        translate([(total_x / 2) - (hole_size_x / 2),
            (total_y / 2) - (hole_size_y / 2),
            -1]) {
                
            cube([hole_size_x, hole_size_y, 2 + floor_thickness]);

        };

 
        
    }
}

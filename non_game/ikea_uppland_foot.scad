$fn = $preview ? 12 : 300;

height = 25;

bottom_diameter = 17.5;
top_diameter = 13.5;
screw_top_diameter = 7;
bolt_hole_diameter = 3.6;
screw_hole_thickness = 8;

difference() {
    union() {
        cylinder(h = height, r1 = bottom_diameter, r2 = top_diameter);
    }
    
    
    union() {
        translate([0, 0, screw_hole_thickness]) {
            cylinder(h = height, r = screw_top_diameter);
        }
        
        cylinder(h = height, r = bolt_hole_diameter);
    }
    
    
}








$fn = $preview ? 12 : 100;


height = 50;
depth = 60;
width = 26;

foot_width = 50;
foot_height = 10;

cavity_width = 21;
cavity_height = 5;
cylinder_radius = 2.4;




difference() {
    union() {
        cube([width, depth, height + cavity_height]);
        
        translate([(width - foot_width) / 2, 0, 0]) {
            cube([foot_width, depth, foot_height]);
        }
    }
    
    union()
    {
        translate([(width - cavity_width) / 2, -1, height]) {
            cube([cavity_width, depth + 2, cavity_height + 1]);
        }
    }
}

translate([width / 2, 20, 0])
{
    cylinder(h = height + 4, r = cylinder_radius);
}
head_thickness = 4.5;
head_radius = 10;
peg_height = 33.25;
peg_radius = 6.1;
taper_height = 3;
taper_radius = 3.5;


difference() {
    union() {


        cylinder(head_thickness, head_radius, head_radius, false);
        cylinder(peg_height-taper_height, peg_radius, peg_radius, false);
        translate([0, 0, peg_height-taper_height]) {
            cylinder(taper_height, peg_radius, taper_radius, false);
        }
        
    }
 
    union() {
        translate([5, -1 - head_radius, -0.1]) {
            cube([head_radius * 2, head_radius * 2, peg_height + 1]);
        }
    }
}





difference() {
    union() {
        cube([40, 8.5, 15]);
        
        
    }
    
    union() {
        
        //big middle cavity
        translate([0, 3, 2]) {
            cube([40, 3.5, 40]);
        }
        
        //chop off tops of hooks
        translate([0, 3, 6]) {
            cube([40, 6, 40]);
        }

        //separate hooks
        translate([8, 3, 0]) {
            cube([24, 6, 40]);
        }

        //nail hole
        translate([20, 3, 8]) {
            rotate([90, 0, 0]) {
                cylinder( h=3,r1=5,r2=3);
            }
        }

    }
}
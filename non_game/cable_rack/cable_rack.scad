
internal_support_depth = 30;
internal_support_width = 50;
internal_support_height = 5;

vertical_support_depth = 30;
vertical_support_width = 3;
vertical_support_height = 20;


rack_depth = 30;
rack_width = 100 + vertical_support_width;


rack_height = internal_support_height;

hole_spacing = 100 / 6;


total_depth = internal_support_depth + vertical_support_depth + rack_depth;



difference() {
    union() {
        cube([vertical_support_width, total_depth, vertical_support_height]);
        
        translate([0, vertical_support_depth + rack_depth, 0]) {
            cube([internal_support_width, internal_support_depth, internal_support_height]);
        }
        
        translate([0 - (rack_width / 2) + (vertical_support_width / 2), 0, 0]) {
            cube([rack_width, rack_depth, rack_height]);
        }
        

        
        
    }
    
    
    union() {
        translate([-1, rack_depth, 0]) {
            cube([vertical_support_width + 2, vertical_support_depth, internal_support_height]);
        }
    
        translate([-60, -10, 10]) {
            rotate([-45, 0, 0]) {
                cube([200, 12, 30]);
            }
        }
        
        
        translate([-6 - hole_spacing * 2, 0, -.1]) {
            translate([0, rack_depth / 2, 0]) {
                cube([4, rack_depth, rack_height + 1]);
           }
           translate([2, rack_depth / 2, 0]) {
                cylinder( h=rack_height + 1, r=3, center = false);
           }
        }
        translate([-6 - hole_spacing * 1, 0, -.1]) {
            translate([0, rack_depth / 2, 0]) {
                cube([4, rack_depth, rack_height + 1]);
           }
           translate([2, rack_depth / 2, 0]) {
                cylinder( h=rack_height + 1, r=3, center = false);
           }
        }
        
        translate([6 + hole_spacing * 2, 0, -.1]) {
            translate([0, rack_depth / 2, 0]) {
                cube([4, rack_depth, rack_height + 1]);
           }
           translate([2, rack_depth / 2, 0]) {
                cylinder( h=rack_height + 1, r=3, center = false);
           }
        }
        translate([6 + hole_spacing * 1, 0, -.1]) {
            translate([0, rack_depth / 2, 0]) {
                cube([4, rack_depth, rack_height + 1]);
           }
           translate([2, rack_depth / 2, 0]) {
                cylinder( h=rack_height + 1, r=3, center = false);
           }
        }
        
    }
    
}





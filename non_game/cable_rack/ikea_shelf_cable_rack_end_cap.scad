include <prism.scad>

 $fn = $preview ? 12 : 72;


rack_length = 104;

rail_width = 18;
rail_height = 15;
wall_thickness = 4;
floor_thickness = 5;
overhang = 25;

extender_height = 50;
extender_thickness = 8;

platform_thickness = 5;
platform_depth = 30;

support_height = 20;
support_thickness = 6;
support_depth = 18;

hole_spacing = 100 / 6;

lip_depth = 5;
lip_height = 2;

slide_buffer = 0;

difference() {
    union() {
        
        translate([0, 3, -3]) {
            cube([rack_length, 12, platform_thickness + 6 + lip_height]);
        }
        
     
     
   }
    
    union() {
        //platform
        translate([0, 0,  -0.25]) {
            cube([rack_length, 12, platform_thickness + 0.25]);
        }
        
 
        // lip
        translate([rack_length, 12, platform_thickness]) {
            rotate([90, 270, 0]) {
                prism(rack_length, lip_height + .25, lip_depth + .5);
            }
        }
    }
    
}




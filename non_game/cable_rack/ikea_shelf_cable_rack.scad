include <prism.scad>

 $fn = $preview ? 12 : 72;


rack_length = 104;

rail_width = 18;
rail_height = 15;
wall_thickness = 4;
floor_thickness = 5;
overhang = 25;

extender_height = 50;
extender_thickness = 8;

platform_thickness = 5;
platform_depth = 30;

support_height = 20;
support_thickness = 6;
support_depth = 18;

hole_spacing = 100 / 6;

lip_depth = 5;
lip_height = 2;


difference() {
    union() {
        //rail surround
        cube([rack_length, rail_width + wall_thickness + overhang + wall_thickness, rail_height + floor_thickness]);
        
        //vertical extender 
        translate([0, rail_width + wall_thickness + overhang, 0]) {
            cube([rack_length, extender_thickness, extender_height + floor_thickness]);
        }
        
        //platform
        
        translate([0, rail_width + wall_thickness + overhang, extender_height + floor_thickness - platform_thickness]) {
            cube([rack_length, platform_depth, platform_thickness]);
        }
        
        
        // triangular support
        translate([support_thickness, rail_width + wall_thickness + overhang + extender_thickness, 
            floor_thickness + extender_height]) {
            rotate([270, 90, 0]) {
                prism(support_thickness, support_height, support_depth);
            }
        }
        
        // triangular support 2
        translate([rack_length, rail_width + wall_thickness + overhang + extender_thickness, 
            floor_thickness + extender_height]) {
            rotate([270, 90, 0]) {
                prism(support_thickness, support_height, support_depth);
            }
        }
 
        // lip
        translate([rack_length, rail_width + wall_thickness + overhang + platform_depth, 
            floor_thickness + extender_height]) {
            rotate([90, 270, 0]) {
                prism(rack_length, lip_height, lip_depth);
            }
        }
     
     
   }
    
    union() {
        //rail cavity
        translate([0, wall_thickness, floor_thickness]) {
            cube([rack_length, rail_width, rail_height]);
        }
        
        // screw holes 
        translate([10, wall_thickness + (rail_width / 2), 0]) {
            cylinder( h=floor_thickness + 1, r=2, center = false);
            cylinder( h=2, r1=4, r2 = 2, center = false);
        }
        
        translate([52, wall_thickness + (rail_width / 2), 0]) {
            cylinder( h=floor_thickness + 1, r=2, center = false);
            cylinder( h=2, r1=4, r2 = 2, center = false);
        }
        
        translate([94, wall_thickness + (rail_width / 2), 0]) {
            cylinder( h=floor_thickness + 1, r=2, center = false);
            cylinder( h=2, r1=4, r2 = 2, center = false);
        }
        
        // cable holes
        translate([hole_spacing, rail_width + wall_thickness + overhang, -.1 + extender_height]) {
            translate([0, platform_depth / 2, 0]) {
                cube([4, platform_depth, platform_thickness + lip_height + 1]);
           }
           translate([2, platform_depth / 2, 0]) {
                cylinder( h=platform_thickness + 1, r=3, center = false);
           }
        }

        translate([hole_spacing * 2, rail_width + wall_thickness + overhang, -.1 + extender_height]) {
            translate([0, platform_depth / 2, 0]) {
                cube([4, platform_depth, platform_thickness + lip_height + 1]);
           }
           translate([2, platform_depth / 2, 0]) {
                cylinder( h=platform_thickness + 1, r=3, center = false);
           }
        }

        translate([hole_spacing * 3, rail_width + wall_thickness + overhang, -.1 + extender_height]) {
            translate([0, platform_depth / 2, 0]) {
                cube([4, platform_depth, platform_thickness + lip_height + 1]);
           }
           translate([2, platform_depth / 2, 0]) {
                cylinder( h=platform_thickness + 1, r=3, center = false);
           }
        }

        translate([hole_spacing * 4, rail_width + wall_thickness + overhang, -.1 + extender_height]) {
            translate([0, platform_depth / 2, 0]) {
                cube([4, platform_depth, platform_thickness + lip_height + 1]);
           }
           translate([2, platform_depth / 2, 0]) {
                cylinder( h=platform_thickness + 1, r=3, center = false);
           }
        }

        translate([hole_spacing * 5, rail_width + wall_thickness + overhang, -.1 + extender_height]) {
            translate([0, platform_depth / 2, 0]) {
                cube([4, platform_depth, platform_thickness + lip_height + 1]);
           }
           translate([2, platform_depth / 2, 0]) {
                cylinder( h=platform_thickness + 1, r=3, center = false);
           }
        }

        
        
    }
    
}




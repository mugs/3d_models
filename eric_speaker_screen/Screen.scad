include <roundedcube.scad>
difference() {
    union() {

        difference() {
            union() {
                roundedcube([83, 70, 3], false, 1.5);

                translate([15.5, 8.5, 3]) {
                    cylinder(4, 3.5, 3.5, false);
                }
                
                translate([83 - 15.5, 70 - 8.5, 3]) {
                    cylinder(4, 3.5, 3.5, false);
                }

            }
            
            union() {
                translate([41.5,35,-1]) {
                    cylinder(5, 30, 30, false);
                }
            
                translate([15.5, 8.5, -1]) {
                    cylinder(10, 2, 2, false);
                }
                translate([15.5, 8.5, 0]) {
                    cylinder(2, 3.5, 2, false);
                }
                translate([83 - 15.5, 70 - 8.5, -1]) {
                    cylinder(10, 2, 2, false);
                }
                translate([83 - 15.5, 70 - 8.5, 0]) {
                    cylinder(2, 3.5, 2, false);
                }
            }
            
        }

        difference() {

            translate([41.5,35,0]) {
                cylinder(3, 25.5, 25.5, false);
            }
            
            translate([41.5,35,-1]) {
                cylinder(5, 23, 23, false);
            }
            
        }

        difference() {

            translate([41.5,35,0]) {
                cylinder(3, 17.5, 17.5, false);
            }
            
            translate([41.5,35,-1]) {
                cylinder(5, 14.5, 14.5, false);
            }
            
        }

        difference() {

            translate([41.5,35,0]) {
                cylinder(3, 9, 9, false);
            }
            
            translate([41.5,35,-1]) {
                cylinder(5, 6.5, 6.5, false);
            }
            
        }


        difference() {
            union() {
                translate([40, 3, 0]) {
                    cube([3, 64, 3]);
                }
         
                translate([40, 33.5, 0]) {
                    rotate([0, 0, 60]) {
                        cube([3, 30, 3]);
                    }
                }
                translate([43, 36.5, 0]) {
                    rotate([0, 0, 240]) {
                        cube([3, 30, 3]);
                    }
                }

                translate([40, 33.5, 0]) {
                    rotate([0, 0, 120]) {
                        cube([3, 30, 3]);
                    }
                }
                translate([43, 36.5, 0]) {
                    rotate([0, 0, 300]) {
                        cube([3, 30, 3]);
                    }
                }


            }
            
            translate([41.5,35,-1]) {
                cylinder(5, 6.5, 6.5, false);
            }

            
        }

    }
    
    translate([41.5,35, 2]) {
        cylinder(5, 31, 33, false);
    }

}
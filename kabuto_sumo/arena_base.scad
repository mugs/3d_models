
$fn = $preview? 20 : 300;

height = 41;
top_radius = 71.5;
bottom_radius = 77.5;
cavity_top_radius = 65;
cavity_bottom_radius = 65;
floor_height = 3;

peg_offset = 66.25;
peg_length = 13.5;
peg_width = 1.5;
peg_height = 3;

difference() {

    union() {
        cylinder(height, bottom_radius, top_radius, false);
        
        translate([0 - peg_offset - peg_width, 0 - (peg_length / 2), 0])
        {
            cube([peg_width, peg_length, peg_height + height]);
        }
        
        translate([0 + peg_offset , 0 - (peg_length / 2), 0])
        {
            cube([peg_width, peg_length, peg_height + height]);
        }

        translate([0 - (peg_length / 2), 0 - peg_offset - peg_width, 0])
        {
            cube([peg_length, peg_width, peg_height + height]);
        }
        
        translate([0 - (peg_length / 2), 0 + peg_offset, 0])
        {
            cube([peg_length, peg_width, peg_height + height]);
        }


    }

    union() {
		translate([0, 0, floor_height]) {
	        cylinder(height, cavity_top_radius, cavity_bottom_radius, false);
    	}
    }

}

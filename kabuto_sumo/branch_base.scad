z0 = 0;
z1 = 42;

x1 = 7.5;
y1 = 80;

x2 = 67.5;
y2 = 80;

x3 = 50;
y3 = 5;

x4 = 25;
y4 = 10;

peg_width = 1.5;
thick_peg_width = 2;
peg_height = 3;

union() {
    points = [
        //top
        [x1, y1, z1], // 0
        [x2, y2, z1], // 1
        [x3, y3, z1], // 2
        [x4, y4, z1], // 3

        // bottom
        [x1, y1, z0], // 4
        [x2, y2, z0], // 5
        [x3, y3, z0], // 6
        [x4, y4, z0], // 7
    ];

    faces = [
        [0, 1, 2, 3], // top
        [7, 6, 5, 4], //bottom
        [0, 3, 7, 4], //left
        [1, 0, 4, 5], //front
        [2, 1, 5, 6], //right
        [3, 2, 6, 7], //back
        
    ];
    polyhedron(points, faces);

    translate([30 , 74 - (peg_width / 2), z1 - 1])
    {
        cube([15, peg_width, peg_height + 1]);
    }

    translate([36.5 , 15 - (peg_width / 2), z1 - 1])
    {
        cube([6, peg_width, peg_height + 1]);
    }
    
    /*
    translate([21.5, 50, z1 - 1])
    {
        cylinder(peg_height + 1, thick_peg_width / 2, peg_width / 2, false);
    }

    translate([54.5, 50, z1 - 1])
    {
        cylinder(peg_height + 1, thick_peg_width / 2, peg_width / 2, false);
    }
    */

}
    

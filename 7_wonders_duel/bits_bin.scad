include <nesting_bin_3_well.scad>

nesting_bin_3_well(
	lid_size = [192, 110, 39],
	compartment1_x = 62,
	compartment2_x = 62,
	bottom_text = "Bits Bits Bits",
	text_size = 15
);
include <card_tray.scad>


card_tray(
	card_size = [68, 44, 32],
	bottom_text = "Age Cards",
	wall_x = 20,
	wall_y = 12,
	text_size = 5
);

include <nesting_bin_lid_3_well.scad>

nesting_bin_lid_3_well(
	lid_size = [192, 110, 39],
  	finger_hole_radius = 15,
    lid_wall_thickness = 1.5,
	bottom_text = "Bits Bits Bits",
	text_size = 15
);

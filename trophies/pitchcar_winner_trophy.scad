
difference() { 
    import("Inspiration_Trophy.stl");

    union() {
        translate([14,2,-0.2]) {
            mirror([1,0,0]) {
                linear_extrude(height = 2) {
                    text("PitchCar", "Liberation Sans", size = 6, "center");
                }
            }
        }
        translate([20,-8,-0.2]) {
            mirror([1,0,0]) {
                linear_extrude(height = 2) {
                    text("2019-09-21", "Liberation Sans", size = 6, "center");
                }
            }
        }
    }
}

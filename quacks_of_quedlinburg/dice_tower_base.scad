include <roundedcube.scad>

x_size = 150;
y_size = 77;
z_size = 25.4;

back_wall_thickness = 2.4;
side_wall_thickness = 2;

floor_thickness = 1.6;


        translate([back_wall_thickness + 70, side_wall_thickness, floor_thickness]) {
            rounded_cube([x_size - side_wall_thickness - back_wall_thickness - 70,
                y_size - (2*side_wall_thickness), 100], false, 7);
        }


difference() {
    cube([x_size, y_size, z_size]);
    
    
    union() {
        translate([back_wall_thickness, (y_size - 65) / 2, floor_thickness]) {
            cube([70, 65, 50]);
        }
    
    }
}



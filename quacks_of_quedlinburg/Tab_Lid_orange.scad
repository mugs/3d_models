include <prism.scad>

box_length = 129;
box_width = 72;
box_height = 37;

side_wall_thickness = 3;

inner_wall_thickness = 2;
bottom_thickness = 2;
roundedness = 7;
compartment1_x = 50;
compartment3_x = 50;
compartment2_x = box_length - compartment1_x - compartment3_x - (inner_wall_thickness * 4);

closer_cube_x = 10;
closer_cube_y = 10;
closer_cube_z = 4;

slider_height = 3;

sphere_radius = 8;
sphere_protrusion = 1;
gap = 0.5;

lid_length = box_length - inner_wall_thickness;
lid_width = box_width - (2*side_wall_thickness) - (gap) + 1;
lid_height = 1.6;

difference() {
	union() {
		cube([lid_length, lid_width, lid_height]);
		translate([0, 0, 0]) {
			rotate([0, 0, 270]) {
				prism(box_length - inner_wall_thickness, 0.6, lid_height);
			}
		}
        translate([lid_length, lid_width, 0]) {
            rotate([0, 0, 90]) {
                prism(box_length - inner_wall_thickness, 0.6, lid_height);
            }
        }
	}

    union() {
        translate([box_length - inner_wall_thickness - (closer_cube_x/2) + 0.15, lid_width / 2, 0 - sphere_radius + sphere_protrusion + 0]) {
            sphere(sphere_radius);
        }

        translate([lid_length, lid_width + 3, 0]) {
            rotate([0, 0, 180]) {
                prism(box_length, 1.5, 1.9);
            }
        }
        difference() {
            translate([8, lid_width / 2, 16 + lid_height - 1.2]) {
                sphere(16);
            }
            translate([-8, lid_width / 2 - 8, lid_height - 8]) {
                cube([16, 16, 16]);
            }
       }
   }
}        


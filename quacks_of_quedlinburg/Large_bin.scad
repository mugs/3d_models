include <roundedcube.scad>

compartment_size = [46,62,100];

difference() {
    cube([148,70,38]);
    union() {
        translate([3,4,2]) {
            roundedcube(compartment_size, false, 7);
        }
        translate([51,4,2]) {
            roundedcube(compartment_size, false, 7);
        }
        translate([99,4,2]) {
            roundedcube(compartment_size, false, 7);
        }
        translate([0,4,33]) {
            cube([145,62,5]);
        }
    }
}
include <roundedcube.scad>

box_length = 131;
box_width = 72;
//box_height = 50;
box_height = 40;


wall_thickness = 2.4;

lid_lip_height = 7;
lid_lip_gap = 2;
lid_lip_thickness = 0.8;
lid_top_thickness = 1;

inner_wall_thickness = 2;
bottom_thickness = 2;
roundedness = 7;
compartment1_x = 50;
compartment2_x = 47.2;
compartment3_x = 50;



difference() {
    cube([box_length, box_width, (lid_lip_height + lid_top_thickness) - lid_lip_gap]);
    translate([lid_lip_thickness, lid_lip_thickness, lid_top_thickness]) {
        cube([box_length - (lid_lip_thickness * 2), box_width - (lid_lip_thickness * 2), lid_lip_height]);
    }

}

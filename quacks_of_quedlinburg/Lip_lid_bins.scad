include <roundedcube.scad>

box_length = 156;
box_width = 72;
box_height = 40;

wall_thickness = 2.4;

lid_lip_height = 7;
lid_lip_thickness = 1.2;

inner_wall_thickness = 2;
bottom_thickness = 2;
roundedness = 7;
compartment1_x = 50;
compartment2_x = 47.2;
compartment3_x = 50;



difference() {
    union() {
        cube([box_length, box_width, box_height - lid_lip_height]);
        translate([lid_lip_thickness, lid_lip_thickness, 0]) {
            cube([box_length - (lid_lip_thickness * 2), box_width - (lid_lip_thickness * 2), box_height]);
        }
    }
    union() {
        translate([wall_thickness, wall_thickness, bottom_thickness]) {
            roundedcube([compartment1_x, box_width - (wall_thickness * 2), 
                box_height + roundedness], false, roundedness);
        }
        translate([wall_thickness + compartment1_x + inner_wall_thickness, wall_thickness, bottom_thickness]) {
            roundedcube([compartment2_x, box_width - (wall_thickness * 2), 
                box_height + roundedness], false, roundedness);
        }
        translate([wall_thickness + compartment1_x + compartment2_x + inner_wall_thickness * 2, 
            wall_thickness, bottom_thickness]) {
            roundedcube([compartment3_x, box_width - (wall_thickness * 2), 
                box_height + roundedness], false, roundedness);
        }
    }
}

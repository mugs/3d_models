include <card_tray_lid.scad>


card_tray_lid(
	card_size = [220, 108, 60],
	bottom_text = "Villain Boards",
    bottom_thickness = 3.2,
    roundedness = 7,
    card_incursion = 3,
    lid_wall_thickness = 2.4,
	wall_x = 70,
	wall_y = 35,
	text_size = 10
);


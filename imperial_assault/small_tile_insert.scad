
total_x = 290;
total_y = 105;
total_z = 77;

top_empty_z = 12;

side_wall = 1.2;
end_wall = 10;
finger_gap = 11;
inner_wall = 3;

//1x2 with connectors on both sides
tile_1x2_a_x = 75;
tile_1x2_a_y = 40;
tile_1x2_a_z = 52;
tile_1x2_a_notch_y = 4;
tile_1x2_a_notch_z = 4;

//1x2 with connectors on one side
tile_1x2_b_x = 89;
tile_1x2_b_y = 33;
tile_1x2_b_z = 52;
tile_1x2_b_notch_y = 4;
tile_1x2_b_notch_z = 4;

//2x2 with connector on one side
tile_2x2_a_x = 8;
tile_2x2_a_y = 52;
tile_2x2_a_z = 59;
tile_2x2_a_notch_y_offset = 29;
tile_2x2_a_notch_y = 13;
tile_2x2_a_notch_z = 12;

//2x2 corner
tile_2x2_b_x = 23;
tile_2x2_b_y = 58;
tile_2x2_b_z = 59;
tile_2x2_b_notch_y_offset = 29;
tile_2x2_b_notch_y = 13;
tile_2x2_b_notch_z = 12;

//2x2 hallway (connectors on opposite sides)
tile_2x2_c_x = 20;
tile_2x2_c_y = 52;
tile_2x2_c_z = 65;
tile_2x2_c_notch_y_offset = 29;
tile_2x2_c_notch_y = 13;
tile_2x2_c_notch_z = 12;

//2x2 hallway (connectors on same side)
tile_2x2_d_x = 32;
tile_2x2_d_y = 52;
tile_2x2_d_z = 65;
tile_2x2_d_notch_y_offset = 29;
tile_2x2_d_notch_y = 13;
tile_2x2_d_notch_z = 12;

//2x2 junction (connectors on 4 sides)
tile_2x2_e_x = 14;
tile_2x2_e_y = 65;
tile_2x2_e_z = 65;
tile_2x2_e_notch_y_offset = 34;
tile_2x2_e_notch_y = 12;
tile_2x2_e_notch_z = 12;


//2x2 junction (connectors on 3 sides)
tile_2x2_f_x = 18;
tile_2x2_f_y = 65;
tile_2x2_f_z = 58;
tile_2x2_f_notch_y_offset = 34;
tile_2x2_f_notch_y = 12;
tile_2x2_f_notch_z = 12;



difference() {
    union() {
        cube([total_x, total_y, total_z]);
    }
    
    union() {
        translate([0, 0, total_z - top_empty_z]) {
            cube([182, total_y - 3, top_empty_z]);
        }
        
        //1x2 with connectors on both sides
        translate([end_wall, side_wall, total_z - tile_1x2_a_z]) {
            difference() {
                cube([tile_1x2_a_x, tile_1x2_a_y, tile_1x2_a_z]);
                
                translate([0, tile_1x2_a_y - tile_1x2_a_notch_y, 0]) {
                    cube([tile_1x2_a_x, tile_1x2_a_notch_y, tile_1x2_a_notch_z]);
                }
            }
        }
        
        //1x2 with connectors on one side
        tile_1x2_b_x_offset = end_wall + tile_1x2_a_x + finger_gap -3;
        translate([tile_1x2_b_x_offset, side_wall, total_z - tile_1x2_b_z]) {
            difference() {
                cube([tile_1x2_b_x, tile_1x2_b_y, tile_1x2_b_z]);
                
                translate([0, tile_1x2_b_y - tile_1x2_b_notch_y, 0]) {
                    cube([tile_1x2_b_x, tile_1x2_b_notch_y, tile_1x2_b_notch_z]);
                }
            }
        }
        
        
        //2x2 with connector on one side
        tile_2x2_a_y_offset = side_wall + tile_1x2_a_y + inner_wall + 3;
        translate([end_wall, tile_2x2_a_y_offset, total_z - tile_2x2_a_z]) {
            difference() {
                cube([tile_2x2_a_x, tile_2x2_a_y, tile_2x2_a_z]);
                
                translate([0, tile_2x2_a_notch_y_offset, 0]) {
                    cube([tile_2x2_b_x, tile_2x2_a_notch_y, tile_2x2_a_notch_z]);
                }
            }
        }
        
        //2x2 hallway (connectors on opposite sides)
        tile_2x2_c_x_offset = end_wall + tile_2x2_a_x + finger_gap;
        tile_2x2_c_y_offset = tile_2x2_a_y_offset;
        translate([tile_2x2_c_x_offset, tile_2x2_c_y_offset, total_z - tile_2x2_c_z]) {
            difference() {
                cube([tile_2x2_c_x, tile_2x2_c_y, tile_2x2_c_z]);
                
                translate([0, tile_2x2_c_notch_y_offset, 0]) {
                    cube([tile_2x2_c_x, tile_2x2_c_notch_y, tile_2x2_c_notch_z]);
                }
            }
        }

        
        //2x2 hallway (connectors on same side)
        tile_2x2_d_x_offset = end_wall + tile_2x2_a_x + finger_gap + tile_2x2_c_x + finger_gap;
        tile_2x2_d_y_offset = tile_2x2_a_y_offset;
        translate([tile_2x2_d_x_offset, tile_2x2_d_y_offset, total_z - tile_2x2_d_z]) {
            difference() {
                cube([tile_2x2_d_x, tile_2x2_d_y, tile_2x2_d_z]);
                
                translate([0, tile_2x2_d_notch_y_offset, 0]) {
                    cube([tile_2x2_d_x, tile_2x2_d_notch_y, tile_2x2_d_notch_z]);
                }
            }
        }



        //2x2 corner
        tile_2x2_b_x_offset = end_wall + tile_2x2_a_x + tile_2x2_c_x + tile_2x2_d_x + (finger_gap * 3) ;
        tile_2x2_b_y_offset = side_wall + tile_1x2_b_y + inner_wall + 3;
        translate([tile_2x2_b_x_offset, tile_2x2_b_y_offset, total_z - tile_2x2_b_z]) {
           difference() {
                cube([tile_2x2_b_x, tile_2x2_b_y, tile_2x2_b_z]);
                
                translate([0, tile_2x2_b_notch_y_offset, 0]) {
                    cube([tile_2x2_b_x, tile_2x2_b_notch_y, tile_2x2_b_notch_z]);
                }
            }
        }
            
        
        //2x2 junction (connectors on 4 sides)
        tile_2x2_e_x_offset = end_wall + tile_2x2_a_x +  tile_2x2_b_x +  tile_2x2_c_x + tile_2x2_d_x + (finger_gap * 4);
        tile_2x2_e_y_offset = side_wall + tile_1x2_b_y + inner_wall;
        translate([tile_2x2_e_x_offset, tile_2x2_e_y_offset, total_z - tile_2x2_e_z]) {
            difference() {
                cube([tile_2x2_e_x, tile_2x2_e_y, tile_2x2_e_z]);
                
                translate([0, tile_2x2_e_notch_y_offset, 0]) {
                    cube([tile_2x2_e_x, tile_2x2_e_notch_y, tile_2x2_e_notch_z]);
                }
            }
        }

        //2x2 junction (connectors on 3 sides)
        tile_2x2_f_x_offset = end_wall + tile_2x2_a_x +  tile_2x2_b_x +  tile_2x2_c_x + tile_2x2_d_x + tile_2x2_e_x + (finger_gap * 5);
        tile_2x2_f_y_offset = side_wall + tile_1x2_b_y + inner_wall;
        translate([tile_2x2_f_x_offset, tile_2x2_f_y_offset, total_z - tile_2x2_f_z]) {
            difference() {
                cube([tile_2x2_f_x, tile_2x2_f_y, tile_2x2_f_z]);
                
                translate([0, tile_2x2_f_notch_y_offset, 0]) {
                    cube([tile_2x2_f_x, tile_2x2_f_notch_y, tile_2x2_f_notch_z]);
                }
            }
        }
        
        translate([185, inner_wall, 1.2]) {
            cube([140, total_y - (inner_wall * 2), total_z]);
        }


    }
}



$fn = 100;

cylinder_height = 10;
cylinder_radius = 10;

arrow_thickness = 1;
arrow_inner_radius = 7.25;

cube_x = 20;
cube_y = 4;
cube_z = 2;


difference() {
    
    union() {
        
        cylinder(h = cylinder_height,r=cylinder_radius);
        
                        
        translate([-8, -3, cylinder_height]) {
            cube([1, 6, 1]);
        }
        translate([7, -3, cylinder_height]) {
            cube([1, 6, 1]);
        }
        
        translate([0, 0, cylinder_height]) {
            
            linear_extrude(height = 1) {
                
                //cw
                //polygon([ [-9.4, -2], [-7.5, 0], [-5.6, -2]]);
                //ccw
                polygon([ [-9.4, 2], [-7.5, 4], [-5.6, 2]]);
                


                text(text = "F", size = 10, halign = "center", valign = "center");
                
                //cw
                //polygon([ [9.4, 2], [7.5, 0], [5.6, 2]]);
                //ccw
                polygon([ [9.4, 2], [7.5, 4], [5.6, 2]]);

            }

            
        }
    
        
        
    }
    
    
    union() {
        
        
    }
    
    
}
include <roundedcube.scad>


$fn = $preview ? 12 : 0;

game_title = "Forest Shuffle";
bottom_text_size = 5;
front_text_size = 5;
front_text_inset = 2;
side_text_size = 5;

card_width = 57;
card_height = 88;
card_thickness = 30;

card_buffer_x = 1;
card_buffer_y = 1;
card_buffer_z = 1;


box_side_thickness = 3.0;
box_bottom_thickness = 1.4;
lip_thickness = 1.6;
lip_height = 25;

slide_buffer = 0.4;

outer_box_roundedness = 3;
cavity_roundedness = 2;

lid_total_x = card_width + (box_side_thickness * 2) + card_buffer_x;
lid_total_y = card_thickness + (box_side_thickness * 2) + card_buffer_y;
lid_total_z = lip_height + box_bottom_thickness;

cavity_total_x = card_width + (lip_thickness * 2) + card_buffer_x + slide_buffer;
cavity_total_y = card_thickness + (lip_thickness * 2) + card_buffer_y + slide_buffer;

z_seam_slot_width = 3;
z_seam_slot_depth = 0.5;

finger_slot_radius = 13;




difference() {
	union() {
		
        //outer box
        translate([0, 0, 0 - outer_box_roundedness]) { 
            difference() { 
                roundedcube([lid_total_x, lid_total_y, lid_total_z + (outer_box_roundedness * 2)], false, outer_box_roundedness);
            
                
                union() {
                    cube([lid_total_x, lid_total_y, outer_box_roundedness]);
                    translate([0, 0, lid_total_z + outer_box_roundedness]) {
                        cube([lid_total_x, lid_total_y, outer_box_roundedness]);
                    }
                }
                
            }
        }        
    }

	union(){
	
        //cavity 
        translate([
            box_side_thickness - lip_thickness - (slide_buffer / 2), 
            box_side_thickness - lip_thickness - (slide_buffer / 2), 
            0 - cavity_roundedness + box_bottom_thickness
        ]) { 
            difference() { 
                roundedcube([cavity_total_x, cavity_total_y, 1000], false, cavity_roundedness);
            
                
                union() {
                    cube([cavity_total_x, cavity_total_y, cavity_roundedness]);
                }
                
            }
        }


        //z-seam slot
        translate([
                box_side_thickness - lip_thickness - z_seam_slot_depth, 
                (lid_total_y - z_seam_slot_width) / 2,
                box_bottom_thickness
            ]) {
            
            cube([z_seam_slot_depth, z_seam_slot_width, 100]);
                
        }


		// bottom text
		translate([lid_total_x / 2, lid_total_y / 2, 0.95]) {
			rotate([180, 0, 180]) {
				linear_extrude(1) {
					text(game_title, bottom_text_size, halign="center");
				}
			}
		}
	}

}
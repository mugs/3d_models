include <roundedcube.scad>

pit_x = 10;
pit_y = 10;
pit_z = 1.5;
pit_top = 2;
bevel_size = 1;

layout = [
    [0, 0, 0, 0, 0],
    [1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
];


module beveled_inset(size = [1, 1, 1], bevel_offset = 1) {
	x1 = 0;
	x2 = size[0];
	x1b = x1 - bevel_offset;
	x2b = x2 + bevel_offset;

	y1 = 0;
	y2 = size[1];
	y1b = y1 - bevel_offset;
	y2b = y2 + bevel_offset;

	z1 = 0;
	z2 = size[2];			


	points = [
	
		//bottom
		[x1, y1, z1], // 0
		[x2, y1, z1], // 1
		[x2, y2, z1], // 2
		[x1, y2, z1], // 3

		// top
		[x1b, y1b, z2], // 4
		[x2b, y1b, z2], // 5
		[x2b, y2b, z2], // 6
		[x1b, y2b, z2], // 7
	];

	faces = [
		[0, 4, 5, 1], //front face
		[1, 5, 6, 2], // outside face
		[2, 6, 7, 3], // back face
		[3, 7, 4, 0], // inside face
		[7, 6, 5, 4], // top
		[0, 1, 2, 3], // bottom        
	];

	polyhedron(points, faces);
}


union() {

	



	difference() {
		roundedcube([70, 80, 10], false, 3, "z");

        union() {
            translate([-1, -1, 2.8]) {
                cube([72,82,10]);
            }
            
            translate([10, 10, pit_top]) {
            	beveled_inset([pit_x, pit_y, pit_z], bevel_size);
            }
            translate([20, 10, pit_top]) {
            	beveled_inset([pit_x, pit_y, pit_z], bevel_size);
            }
        }
	}
}
